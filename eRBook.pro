APP_NAME = eRBook

QT += network sql
CONFIG += qt warn_on cascades10
LIBS += -lbbdevice -lbbsystem -lbbplatformbbm -lbbplatform -lbbdata -lbb -lscreen -lcrypto -lcurl -lpackageinfo
DEFINES += TESTERS
lupdate_inclusion {
    SOURCES += ../assets/*.qml
    SOURCES += ../assets/Delegates/*.qml
    SOURCES += ../assets/Pages/*.qml
    SOURCES += ../assets/Common/*.qml
}

include(config.pri)