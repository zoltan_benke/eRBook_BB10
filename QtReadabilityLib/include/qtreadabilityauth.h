/*
 *	QtReadability - A Readability.com library for Qt
 *
 *	Copyright (C) 2014 Zoltán Benke <benecore@devpda.net>
 *                     			      http://devpda.net
 *
 *
 *	Redistribution and use in source and binary forms, with or without
 *	modification, are permitted provided that the following conditions are met:
 *	1. Redistributions of source code must retain the above copyright
 *	notice, this list of conditions and the following disclaimer.
 *	2. Redistributions in binary form must reproduce the above copyright
 *	notice, this list of conditions and the following disclaimer in the
 *	documentation and/or other materials provided with the distribution.
 *	3. All advertising materials mentioning features or use of this software
 *	must display the following acknowledgement:
 *	This product includes software developed by the Zoltán Benke.
 *	4. Neither the name of the Zoltán Benke nor the
 *	names of its contributors may be used to endorse or promote products
 *	derived from this software without specific prior written permission.
 *
 *	THIS SOFTWARE IS PROVIDED 'AS IS' AND ANY
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *	DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef QTREADABILITYAUTH_H
#define QTREADABILITYAUTH_H

#include "qtreadability_export.h"
#include <QObject>
#include <QUrl>
#ifdef QT5
#include <QUrlQuery>
#endif
#include <QMultiMap>

class QtReadabilityAuthPrivate;
class QTREADABILITYSHARED_EXPORT QtReadabilityAuth : public QObject
{
    Q_OBJECT
public:
    explicit QtReadabilityAuth(QObject *parent = 0);
    virtual ~QtReadabilityAuth();


    enum RequestType{
        REQUEST_TOKEN,
        ACCESS_TOKEN,
        XAUTH_LOGIN,
        AUTHORIZED
    };

    enum HttpMethod{
        GET,
        POST,
        PUT,
        DELETE,
        HEAD
    };


public slots:
    // Setters
    void setType(RequestType type);
    void setConsumerKey(const QString& consumerKey);
    void setConsumerSecret(const QString& consumerSecret);
    void setCallbackUrl(const QUrl& callbackUrl);
    void setToken(const QString& token);
    void setTokenSecret(const QString& tokenSecret);
    void setVerifier(const QString& verifier);

    // Getters
    QtReadabilityAuth::RequestType type() const;
    QString token() const;
    QString tokenSecret() const;



    QByteArray generateAuthHeader(const QUrl &requestUrl,
                                  HttpMethod httpMethod = GET,
                                  const QMultiMap<QString, QString> &params = QMultiMap<QString, QString>());

private slots:
    QString generateSignature(const QUrl& requestUrl, const QMultiMap<QString, QString>& requestParameters, HttpMethod method) const;
    QString hmac_sha1(const QString& message, const QString& key) const;

private:
    QtReadabilityAuthPrivate * const d_ptr;
    Q_DECLARE_PRIVATE(QtReadabilityAuth)
    Q_DISABLE_COPY(QtReadabilityAuth)
};

#endif // QTREADABILITYAUTH_H
