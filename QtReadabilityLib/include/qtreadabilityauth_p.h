/*
 *	QtReadability - A Readability.com library for Qt
 *
 *	Copyright (C) 2014 Zoltán Benke <benecore@devpda.net>
 *                     			      http://devpda.net
 *
 *
 *	Redistribution and use in source and binary forms, with or without
 *	modification, are permitted provided that the following conditions are met:
 *	1. Redistributions of source code must retain the above copyright
 *	notice, this list of conditions and the following disclaimer.
 *	2. Redistributions in binary form must reproduce the above copyright
 *	notice, this list of conditions and the following disclaimer in the
 *	documentation and/or other materials provided with the distribution.
 *	3. All advertising materials mentioning features or use of this software
 *	must display the following acknowledgement:
 *	This product includes software developed by the Zoltán Benke.
 *	4. Neither the name of the Zoltán Benke nor the
 *	names of its contributors may be used to endorse or promote products
 *	derived from this software without specific prior written permission.
 *
 *	THIS SOFTWARE IS PROVIDED 'AS IS' AND ANY
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *	DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef QTREADABILITYAUTH_P_H
#define QTREADABILITYAUTH_P_H


#include "qtreadabilityauth.h"


class QTREADABILITYSHARED_EXPORT QtReadabilityAuthPrivate
{
public:
    QtReadabilityAuthPrivate();
    virtual ~QtReadabilityAuthPrivate();


    QtReadabilityAuth::RequestType requestType;
    QString consumerKey;
    QString consumerSecret;
    QString scope;
    QUrl callbackUrl;
    QString oauthToken;
    QString oauthTokenSecret;
    QString oauthVerifier;



};

#endif // QTREADABILITYAUTH_P_H
