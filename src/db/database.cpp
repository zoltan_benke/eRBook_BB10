/*
 * database.cpp
 *
 *  Created on: 30.8.2013
 *      Author: Benecore
 */

#include "database.h"
#include <bb/system/SystemDialog>


using namespace bb::system;


Database *Database::_instance = NULL;

Database::Database(QObject *parent) :
												QObject(parent)
{
	// TODO Auto-generated constructor stub
	_db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
	_db->setDatabaseName("data/database.db");
	bool ok = _db->open();
	if(ok){
		//alert("DatabaseManager: db opened.");
	}else{
		alert("DatabaseManager: db open error.");
	}
	createTables();
}

Database::~Database()
{
	// TODO Auto-generated destructor stub
	delete _db;
}



Database *Database::instance()
{
	if (!_instance)
		_instance = new Database;
	return _instance;
}


void Database::destroy()
{
	if (_instance){
		delete _instance;
		_instance = NULL;
	}
}



void Database::alert(const QString &message)
{
	/*
	SystemDialog *dialog; // SystemDialog uses the BB10 native dialog.
	dialog = new SystemDialog(tr("OK"), 0); // New dialog with on 'Ok' button, no 'Cancel' button
	dialog->setTitle(tr("Alert")); // set a title for the message
	dialog->setBody(message); // set the message itself
	dialog->setDismissAutomatically(true); // Hides the dialog when a button is pressed.

	// Setup slot to mark the dialog for deletion in the next event loop after the dialog has been accepted.
	bool ok = connect(dialog, SIGNAL(finished(bb::system::SystemUiResult::Type)), dialog, SLOT(deleteLater()));
	Q_ASSERT(ok);
	Q_UNUSED(ok);
	dialog->show();
	 */
	qDebug() << message;
}


/* -------------------------------- */
void Database::createTables()
{
	_db->exec("CREATE TABLE IF NOT EXISTS auth(token TEXT, secret TEXT)");
	_db->exec("CREATE TABLE IF NOT EXISTS cacher (id TEXT UNIQUE, content TEXT, position NUMBER)");
	/*if (_db->tables().contains(QLatin1String("cacher"))){
		qDebug() << Q_FUNC_INFO << "SHORT URL TABLE EXISTS";
	}else{
		qDebug() << Q_FUNC_INFO << "SHORT URL TABLE NOT EXISTS";
	}*/
	_db->exec("ALTER TABLE cacher ADD shortUrl TEXT");
}



bool Database::isAuthorized() const
{
	if (!_db->isOpen())
		_db->open();
	QVariant value;

	QSqlQuery query;
	query.exec("SELECT token FROM auth");
	while (query.next()){
		value = query.value(0);
	}
	return !value.isNull();
}


QList<QVariant> Database::authData()
{
	if (!_db->isOpen())
		_db->open();

	QList<QVariant> list;

	QSqlQuery query;

	query.exec("SELECT * FROM auth");

	while(query.next()){
		list << query.value(0) << query.value(1);
	}
	return list;
}


void Database::setAuth(const QVariant &token, const QVariant &secret)
{
	if (!_db->isOpen())
		_db->open();
	QSqlQuery query;
	query.prepare("INSERT INTO auth VALUES(?,?)");
	query.addBindValue(token);
	query.addBindValue(secret);

	bool success = query.exec();

	if (success){
		alert("Auth data has been set");
	}else{
		alert("Auth data unable set");
	}
	emit authorizedChanged(isAuthorized());
	emit authChanged(token, secret);
}


bool Database::resetAuth()
{
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;

	bool success = query.exec("DELETE FROM auth");

	emit authChanged();
	emit authorizedChanged(isAuthorized());
	clearDatabase();
	emit resetAuthDone();
	return success;
}


bool Database::clearDatabase()
{
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;

	bool success = query.exec("DELETE FROM cacher");

	return success;
}



void Database::cache(const QVariant &id, const QVariant &content)
{
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;
	query.prepare("INSERT OR REPLACE INTO cacher (id, content) VALUES(?, ?)");
	query.addBindValue(id);
	query.addBindValue(content);

	bool success = query.exec();

	if (!success)
		alert("Unable to add Article to cacher table");
}



bool Database::isCached(const QVariant &id)
{
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;
	query.prepare("SELECT id FROM cacher WHERE id = ?");
	query.addBindValue(id);

	if (query.exec() && query.next())
		return true;
	return false;
}



bool Database::clearCache()
{
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;
	query.prepare("DELETE FROM cacher");

	bool success = query.exec();

	if (!success){
		alert("Unable clear cacher table");
		return false;
	}
	return true;
}


int Database::cacheCount()
{
	int count;
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;
	query.prepare("SELECT COUNT(*) FROM cacher");

	bool success = query.exec();

	if (!success)
		alert("Unable clear cahcer table");

	while(query.next()){
		count = query.value(0).toInt();
	}
	return count;
}


QVariant Database::getCache(const QVariant &id)
{
	QVariant value;
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;
	query.prepare("SELECT content FROM cacher WHERE id = ?");
	query.addBindValue(id);

	bool success = query.exec();
	while (query.next()){
		value = query.value(0);
	}
	if (!success){
		alert("UNABLE TO GET content from CACHER table");
	}
	return value;
}


bool Database::removeCache(const QVariant &id)
{
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;
	query.prepare("DELETE FROM cacher WHERE id = ?");
	query.addBindValue(id);

	bool success = query.exec();

	if (!success){
		alert("UNABLE TO DELETE content value to CACHER table");
		return false;
	}
	return true;
}



void Database::updatePosition(const QVariant &id, const QVariant &position)
{
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;
	query.prepare("UPDATE cacher SET position = ? WHERE id = ?");
	query.addBindValue(position);
	query.addBindValue(id);

	bool success = query.exec();

	if (!success){
		alert("UNABLE TO UPDATE POSITION");
	}
}



float Database::getPosition(const QVariant &id)
{

	float position;
	if (!_db->isOpen())
		_db->open();
	QSqlQuery query;
	query.prepare("SELECT position FROM cacher WHERE id = ?");
	query.addBindValue(id);

	bool success = query.exec();

	while (query.next()){
		position = query.value(0).toFloat();
	}
	if (!success){
		alert("UNABLE TO GET POSITION");
	}
	return position;
}



QVariant Database::shortUrl(const QVariant &id)
{
	QVariant shortUrl;
	if (!_db->isOpen())
		_db->open();
	QSqlQuery query;
	query.prepare("SELECT shortUrl FROM cacher WHERE id = ?");
	query.addBindValue(id);

	bool success = query.exec();

	while (query.next()){
		shortUrl = query.value(0);
	}
	if (!success){
		alert("UNABLE TO GET SHORT URL");
	}
	return shortUrl;
}



bool Database::shortUrlExists(const QVariant &id)
{
	QVariant shortUrl;
	if (!_db->isOpen())
		_db->open();

	QSqlQuery query;
	query.prepare("SELECT shortUrl FROM cacher WHERE id = ?");
	query.addBindValue(id);

	if (query.exec() && query.next())
		shortUrl = query.value(0);

	if (shortUrl.isNull())
		return false;
	return true;
}


bool Database::setShortUrl(const QVariant& id, const QVariant& url)
{
	if (!_db->isOpen())
		_db->open();
	QSqlQuery query;
	query.prepare("UPDATE cacher SET shortUrl = ? WHERE id = ?");
	query.addBindValue(url);
	query.addBindValue(id);

	bool success = query.exec();

	if (!success){
		alert("UNABLE TO INSERT SHORTURL to CACHER table");
	}

	return success;
}
