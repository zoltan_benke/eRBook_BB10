#ifndef HELPERUTILS_H
#define HELPERUTILS_H

#include <QObject>
#include <Qt/qtextdocument.h>
#include <QRegExp>
#include <bb/system/SystemDialog>
#include <bb/system/SystemUiResult>
#include <bb/system/SystemToast>

using namespace bb::system;

class HelperUtils : public QObject
{
    Q_OBJECT
public:
    explicit HelperUtils(QObject *parent = 0);
    virtual ~HelperUtils();


    static HelperUtils *instance();
    static void destroy();


    Q_INVOKABLE QString formatText(const QString &text);
    Q_INVOKABLE QString removeHref(QString content);

    Q_INVOKABLE
    void showDialog(const QString &confirmLabel, const QString &cancelLabel, const QString &title, const QString &message);
    Q_INVOKABLE
    void showToast(const QString &message);
    Q_INVOKABLE
    bool validUrl(QString url);

signals:
	void okClicked();

private slots:
	void dialogFinished(bb::system::SystemUiResult::Type result);


private:
	static HelperUtils *_instance;
	bool ok;
	SystemDialog *dialog;
	SystemToast *toast;

};

#endif // HELPERUTILS_H
