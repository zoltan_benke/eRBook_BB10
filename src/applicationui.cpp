/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"


ApplicationUI::ApplicationUI(Application *app) :
QObject(app), _loading(false), _isOffline(false), _syncing(false), _updating(false),
_invokeManager(new InvokeManager(this)), _fromAddPage(false)
{

#ifdef TESTERS
	QStringList pins;
	pins << "0x2ABFF247" << "0x2ABABDFD" << "0x2ab9bd66" << "0x2abad6ec" << "0x24F1F7D2" << "0x24CBF717" << \
			"0x2ABAD648" << "0x2AB8866B" << "0x2B598FA6" << "0x2B30C1BE";/* << "0xffffa392"*/;
	HardwareInfo hwInfo;
	qDebug() << "MY PIN" << hwInfo.pin();
	if (!pins.contains(hwInfo.pin(), Qt::CaseInsensitive)){
		qDebug() << "EXIT APP";
		Application::instance()->requestExit();
	}
#endif

	// Listen to incoming invocation requests
	bool ok = connect(_invokeManager, SIGNAL(invoked(const bb::system::InvokeRequest&)),
			this, SLOT(handleInvoke(const bb::system::InvokeRequest&)));
	Q_ASSERT(ok);
	ok = connect(_invokeManager,
			SIGNAL(cardResizeRequested(const bb::system::CardResizeMessage&)),
			this, SLOT(resized(const bb::system::CardResizeMessage&)));
	Q_ASSERT(ok);
	ok = connect(_invokeManager,
			SIGNAL(cardPooled(const bb::system::CardDoneMessage&)), this,
			SLOT(pooled(const bb::system::CardDoneMessage&)));
	Q_ASSERT(ok);


	_api = new QtReadability("Benecore2", "gXahkxWqxmCQKbjvZkxDztBZkrNYMfaf", this);
	_db = Database::instance();
	_settings = Settings::instance();
#ifdef TESTERS
	_settings->setIsPro(true);
#endif
	_cacher = Cacher::instance();

	_sizeHelper = SizeHelper::instance();

	_helper = HelperUtils::instance();
	_model = new GroupDataModel;
	//_model->setSortingKeys(QStringList() << _settings->sortingKeys().toString());
	_model->setGrouping(ItemGrouping::None);
	//_model->setSortedAscending(_settings->sortAscending());
	_tagsModel = new GroupDataModel;
	setTagsGrouping(_settings->tagsGrouping() ? "first" : "none");
	setTagsSortAscending(_settings->tagsSortAscending());
	_tagsModel->setSortingKeys(QStringList() << "text" << "applied_count");

	syncDialog = SyncDialog::instance();
	connect(syncDialog, SIGNAL(canceled()), this, SLOT(canceledSync()));
	_statusHandler = new StatusEventHandler;
	connect(_statusHandler, SIGNAL(networkStatusUpdated(bool,QString)), this,
			SLOT(networkStatusUpdated(bool,QString)));

	_invoker = new Invoker;

	/** Is offline **/
	if (_cacher->getLocal().isEmpty()){
		_isOffline = false;
	}else{
		_isOffline = true;
	}

	/** Registration handler **/
	const QString uuid(QLatin1String("669484af-4c57-491e-be56-b1591a4cd687"));

	_registrationHandler = new RegistrationHandler(uuid);

	_inviteDownload = new InviteToDownload(_registrationHandler->context());

	// prepare the localization
	m_pTranslator = new QTranslator(this);
	m_pLocaleHandler = new LocaleHandler(this);

	bool res = QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()));
	// This is only available in Debug builds
	Q_ASSERT(res);
	// Since the variable is not used in the app, this is added to avoid a
	// compiler warning
	Q_UNUSED(res);

	// initial load
	onSystemLanguageChanged();

	if (_db->authData().count()){
		qDebug() << "AUTH IS SET";
		QList<QVariant> tokens = _db->authData();
		qDebug() << "TOKEN: " << tokens.at(0).toByteArray() << endl << \
				"TOKEN_SECRET: " << tokens.at(1).toByteArray();
		_api->setToken(tokens.at(0).toString());
		_api->setTokenSecret(tokens.at(1).toString());
	}

	connect(_api, SIGNAL(accessTokenReceived(QMap<QString, QString>)), this,
			SLOT(loginDone(QMap<QString, QString>)));
	connect(_api, SIGNAL(responseReceived(QByteArray)), this,
			SLOT(requestFinished(QByteArray)));
	connect(_api, SIGNAL(requestTokenReceived(QMap<QString, QString>)),
			this,
			SLOT(requestTokenReceived(QMap<QString, QString>)));
	connect(_api, SIGNAL(authorizationUrlReceived(QUrl)),
			this,
			SLOT(authorizationUrlReceived(QUrl)));


	switch (_invokeManager->startupMode()) {
	case ApplicationStartupMode::LaunchApplication:
		_isInvokation = false;
		m_startupMode = tr("Launch");
		initFullUI();
		qDebug() << "LAUNCH APPLICATION";
		break;
	case ApplicationStartupMode::InvokeApplication:
		m_startupMode = tr("Invoke");
		qDebug() << "INVOKE APPLICATION";
		// Wait for invoked signal to determine and initialize the appropriate UI
		break;
	case ApplicationStartupMode::InvokeCard:
		m_startupMode = tr("Card");
		_isInvokation = true;
		qDebug() << "INVOKE CARD";
		initCardUI();
		// Wait for invoked signal to determine and initialize the appropriate UI
		break;
	default:
		break;
	}
}



ApplicationUI::~ApplicationUI()
{
	delete m_pTranslator;
	delete m_pLocaleHandler;
	delete _registrationHandler;
	delete _inviteDownload;
	delete _sizeHelper;
	delete _db;
	delete _api;
	delete _settings;
	delete _cacher;
	delete _helper;
	ThemeSettings::destroy();
	delete _statusHandler;
	delete _invokeManager;
	delete syncDialog;
	delete _invoker;
	delete _tagsModel;
}


void ApplicationUI::onSystemLanguageChanged()
{
	QCoreApplication::instance()->removeTranslator(m_pTranslator);
	// Initiate, load and install the application translation files.
	QString locale_string = QLocale().name();
	QString file_name = QString("eRBook_%1").arg(locale_string);
	if (m_pTranslator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_pTranslator);
	}
}



void ApplicationUI::initFullUI()
{
	qDebug() << "FULL UI INITIALIZE";
	qmlRegisterType<WebImageView>("com.devpda.tools", 1, 2, "RemoteImage");
	qmlRegisterType<Timer>("com.devpda.tools", 1, 2, "Timer");
	qmlRegisterType<Invoker>("com.devpda.tools", 1, 2, "Invoker");
	// Create scene document from main.qml asset, the parent is set
	// to ensure the document gets destroyed properly at shut down.
	QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

	qml->setContextProperty("App", this);
	qml->setContextProperty("Api", _api);
	qml->setContextProperty("Database", _db);
	qml->setContextProperty("Settings", _settings);
	qml->setContextProperty("BBM", _registrationHandler);
	qml->setContextProperty("Invite", _inviteDownload);
	qml->setContextProperty("SizeHelper", _sizeHelper);
	qml->setContextProperty("Helper", _helper);
	qml->setContextProperty("ThemeSettings", ThemeSettings::instance());
	qml->setContextProperty("Cacher", _cacher);

	// Create root object for the UI
	AbstractPane *root = qml->createRootObject<AbstractPane>();


	QmlDocument *qmlCover = QmlDocument::create("asset:///Cover.qml").parent(this)
																																																																																																																					.property("App", this)
																																																																																																																					.property("SizeHelper", _sizeHelper);
	if (!qmlCover->hasErrors()) {
		// Create the QML Container from using the QMLDocument.
		Container *coverContainer = qmlCover->createRootObject<Container>();

		// Create a SceneCover and set the application cover
		SceneCover *sceneCover = SceneCover::create().content(coverContainer);
		Application::instance()->setCover(sceneCover);
	}else{
		qDebug() << Q_FUNC_INFO << "unable set cover";
	}


	// Set created root object as the application scene
	Application::instance()->setScene(root);

	new QmlBeam(this);
}


void ApplicationUI::initCardUI()
{
	qDebug() << "CARD UI INITIALIZE";
	qmlRegisterType<WebImageView>("com.devpda.tools", 1, 2, "RemoteImage");
	qmlRegisterType<Timer>("com.devpda.tools", 1, 2, "Timer");
	qmlRegisterType<Invoker>("com.devpda.tools", 1, 2, "Invoker");
	// Create scene document from main.qml asset, the parent is set
	// to ensure the document gets destroyed properly at shut down.
	QmlDocument *qml = QmlDocument::create("asset:///Invocation/AddPage.qml").parent(this);

	qml->setContextProperty("App", this);
	qml->setContextProperty("Api", _api);
	qml->setContextProperty("Settings", _settings);

	// Create root object for the UI
	AbstractPane *root = qml->createRootObject<AbstractPane>();

	// Set created root object as the application scene
	Application::instance()->setScene(root);
}


void ApplicationUI::handleInvoke(const bb::system::InvokeRequest& request)
{
	//"com.devpda.eRBook.article.add"
	// Copy data from incoming invocation request to properties
	m_source =
			QString::fromLatin1("%1 (%2)").arg(request.source().installId()).arg(
					request.source().groupId());
	m_target = request.target();
	m_action = request.action();
	m_mimeType = request.mimeType();
	m_uri = request.uri().toString();
	m_data = QString::fromUtf8(request.data());

	// Signal that the properties have changed
	emit requestChanged();
}


void ApplicationUI::resized(const bb::system::CardResizeMessage& request) {
	Q_UNUSED(request)

							_invokeManager->cardResized(request);																																																																																																						m_status = tr("Resized");
	emit statusChanged();
}


void ApplicationUI::pooled(const bb::system::CardDoneMessage& request) {
	Q_UNUSED(request)
																																																																																																																							m_status = tr("Pooled");
	m_source = m_target = m_action = m_mimeType = m_uri = m_data = tr("--");
	emit statusChanged();
	emit requestChanged();
}


void ApplicationUI::networkStatusUpdated(bool status, QString type)
{
	_online = status;
	_netType = type;
	qDebug() << "ONLINE: " << _online << endl << \
			"NETWORK TYPE: " << _netType;
	emit onlineChanged(_online, _netType);
}


void ApplicationUI::setSortingKeys(const QString &sortingKey)
{
	_model->setSortingKeys(QStringList() << sortingKey);
	qDebug() << "CURRENT SORTING KEYS: " << _model->sortingKeys();
}


void ApplicationUI::setSortAscending(bool ascending)
{
	_model->setSortedAscending(ascending);
}


void ApplicationUI::setGrouping(const QString &grouping)
{
	if (grouping == "first"){
		_model->setGrouping(ItemGrouping::ByFirstChar);
	}
	else if (grouping == "full"){
		_model->setGrouping(ItemGrouping::ByFullValue);
	}
	else if (grouping == "none"){
		_model->setGrouping(ItemGrouping::None);
	}
}

void ApplicationUI::setTagsSortingKeys(const QStringList &sortingKeys)
{
	_tagsModel->setSortingKeys(sortingKeys);
	qDebug() << "CURRENT SORTING KEYS: " << _tagsModel->sortingKeys();
}


void ApplicationUI::setTagsSortAscending(bool ascending)
{
	_tagsModel->setSortedAscending(ascending);
}


void ApplicationUI::setTagsGrouping(const QString &grouping)
{
	if (grouping == "first"){
		_tagsModel->setGrouping(ItemGrouping::ByFirstChar);
	}
	else if (grouping == "full"){
		_tagsModel->setGrouping(ItemGrouping::ByFullValue);
	}
	else if (grouping == "none"){
		_tagsModel->setGrouping(ItemGrouping::None);
	}
}


void ApplicationUI::showReading() {
	setLoading(true);

	QString tags;
	_model->clear();
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(_cacher->getLocal()).toList();
	qDebug() << "LIST COUNT:" << list.count();
	foreach(const QVariant &item, list){
		if (!item.toMap().value("archive").toBool()){
			tags.clear();
			QVariantMap article = item.toMap();
			article.insert("title", article.value("article").toMap().value("title"));
			foreach(const QVariant &tag, article.value("tags").toList()){
				tags += tag.toMap().value("text").toString() + ", ";
			}
			tags.chop(2);
			article.insert("tagy", tags);
			_model->insert(article);
		}
	}
	emit modelSizeChanged(modelSize());
	setLoading(false);
}


void ApplicationUI::showFavorites() {
	setLoading(true);

	QString tags;
	_model->clear();
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(_cacher->getLocal()).toList();
	foreach(const QVariant &item, list){
		if (item.toMap().value("favorite").toBool()){
			tags.clear();
			QVariantMap article = item.toMap();
			article.insert("title", article.value("article").toMap().value("title"));
			foreach(const QVariant &tag, article.value("tags").toList()){
				tags += tag.toMap().value("text").toString() + ", ";
			}
			tags.chop(2);
			article.insert("tagy", tags);
			_model->insert(article);
		}
	}
	emit modelSizeChanged(modelSize());
	setLoading(false);
}


void ApplicationUI::showArchive() {
	setLoading(true);

	QString tags;
	_model->clear();
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(_cacher->getLocal()).toList();
	foreach(const QVariant &item, list){
		if (item.toMap().value("archive").toBool()){
			tags.clear();
			QVariantMap article = item.toMap();
			article.insert("title", article.value("article").toMap().value("title"));
			foreach(const QVariant &tag, article.value("tags").toList()){
				tags += tag.toMap().value("text").toString() + ", ";
			}
			tags.chop(2);
			article.insert("tagy", tags);
			_model->insert(article);
		}
	}
	emit modelSizeChanged(modelSize());
	setLoading(false);
}


void ApplicationUI::showTags() {
	setLoading(true);

	QByteArray tags = _cacher->getTags();
	if (tags.isEmpty()){
		getTags();
	}else{
		_tagsModel->clear();
		JsonDataAccess jda;
		QVariantList list = jda.loadFromBuffer(tags).toMap().value("tags").toList();
		_tagsModel->insertList(list);
		emit tagsModelChanged(tagsSize());
		setLoading(false);
	}
}


void ApplicationUI::showTagsBookmark(const QVariantList &bookmarksId) {
	setLoading(true);

	static QVariantList temp;
	if (!bookmarksId.isEmpty()){
		temp = bookmarksId;
	}
	QString tags;
	_model->clear();
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(_cacher->getLocal()).toList();
	foreach(const QVariant &item, list){
		QVariantMap bookmark = item.toMap();
		if (temp.contains(bookmark.value("id"))){
			bookmark.insert("tagy", "");
			_model->insert(bookmark);
		}
	}
	emit modelSizeChanged(modelSize());
	setLoading(false);
}


void ApplicationUI::syncContent()
{
	syncCanceled = false;
	_api->setSynchronous(false);
	QByteArray articles = _cacher->getLocal();
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(articles).toList();
	qDebug() << "Cache count:" << _db->cacheCount() << " | " << "LIST COUNT:" << list.count();
	setSyncing(true);
	syncDialog->setBody(tr("Starting synchronization"));
	syncDialog->show();
	int total = list.count() - _db->cacheCount();
	for (int i = 0; i != list.count() && !syncCanceled; ++i){
		QVariantMap article = list.at(i).toMap();
		if (!_db->isCached(article.value("article").toMap().value("id"))){
			syncDialog->setBody(tr("Syncing article"));
			syncDialog->setProgress((i*100)/list.count());
			syncDialog->setStatus(QString("%1/%2").arg(i).arg(total));
			QEventLoop loop;
			loop.connect(_api, SIGNAL(responseReceived(QByteArray)), SLOT(quit()));
			_api->getArticle(article.value("article").toMap().value("id").toString());
			loop.exec();
		}
		//emit currentSyncing(i, list.count());
	}
	syncDialog->close();
	if (syncCanceled){
		_helper->showToast(tr("Synchronization canceled. Some of the articles was not successfully synced"));
	}else{
		//_helper->showToast(tr("Articles was synced sucessfully"));
	}
	emit syncingDone();
	_api->setSynchronous(true);
	setSyncing(false);
}


void ApplicationUI::requestTokenReceived(QMap<QString, QString> response)
{
	Q_UNUSED(response);
	_api->getAuthorization();
}


void ApplicationUI::authorizationUrlReceived(QUrl authorizationUrl)
{
	emit authorizationUrlDone(authorizationUrl);
}


void ApplicationUI::loginDone(QMap<QString, QString> tokens)
{
	const QtReadability::OAuthError &error = _api->lastError();
	if (!error){
		qDebug() << "TOKEN: " << tokens.value("oauth_token") << endl << \
				"TOKEN_SECRET: " << tokens.value("oauth_token_secret");
		_db->setAuth(tokens["oauth_token"], tokens["oauth_token_secret"]);
	}else{
		if (_api->errorCode() == 500){
			internalServerError();
		}
	}
}


void ApplicationUI::requestFinished(QByteArray response)
{
	const QtReadability::ApiRequest &request = _api->apiRequest();
	switch(request){
	case QtReadability::GET_ARTICLE:
		parseArticle(response);
		break;
	case QtReadability::GET_BOOKMARKS:
		parseBookmarks(response);
		break;
	case QtReadability::ADD_BOOKMARK:
		parseAddBookmark(response);
		break;
	case QtReadability::UPDATE_BOOKMARK:
		parseUpdateBookmark(response);
		break;
	case QtReadability::DELETE_BOOKMARK:
		parseRemoveBookmark(response);
		break;
	case QtReadability::GET_BOOKMARK:
		break;
	case QtReadability::GET_BOOKMARK_TAGS:
		break;
	case QtReadability::DELETE_TAG_FROM_BOOKMARK:
		parseDeleteBookmarkTag(response);
		break;
	case QtReadability::GET_TAGS:
		parseGetTags(response);
		break;
	case QtReadability::GET_TAG:
		break;
	case QtReadability::DELETE_TAG:
		parseDeleteTag(response);
		break;
	case QtReadability::GET_USER_INFO:
		parseUserInfo(response);
		break;
	case QtReadability::GET_SHORT_URL:
		parseShortUrl(response);
		break;
	case QtReadability::ADD_TAGS_TO_BOOKMARK:
		parseSetTags(response);
		break;
	default:
		break;
	}
}


void ApplicationUI::error(int error, QByteArray errorString)
{
	qDebug() << "ERROR: " << error << endl << \
			"STRING: " << errorString;
	setLoading(false);
	setUpdating(false);
	switch(error){
	case QTREADABILITY_BAD_REQUEST:
		qDebug() << QTREADABILITY_BAD_REQUEST << ": BAD request";
		_helper->showToast(tr("Bad request. Check your URL"));
		break;
	case QTREADABILITY_AUTH_REQUIRED:
		qDebug() << QTREADABILITY_AUTH_REQUIRED << ": Invalid credentials | Auth required";
		_helper->showToast(tr("Invalid login. Check you credentials and try again"));
		break;
	case QTREADABILITY_FORBIDDEN:
		qDebug() << QTREADABILITY_FORBIDDEN << ": Forbidden";
		break;
	case QTREADABILITY_NOT_FOUND:
		qDebug() << QTREADABILITY_NOT_FOUND << ": Not found";
		break;
	case QTREADABILITY_INTERNAL_SERVER_ERROR:
		qDebug() << QTREADABILITY_INTERNAL_SERVER_ERROR << ": Internal server error";
		_helper->showToast(tr("Internal server error. Try again later"));
		break;
	case QTREADABILITY_ARTICLE_CREATED:
		qDebug() << QTREADABILITY_ARTICLE_CREATED << ": Article created";
		break;
	case QTREADABILITY_ARTICLE_RECREATED:
		qDebug() << QTREADABILITY_ARTICLE_RECREATED << ": Article re-created";
		break;
	case QTREADABILITY_ARTICLE_EXISTS:
		qDebug() << QTREADABILITY_ARTICLE_EXISTS << ": Article exists";
		_helper->showToast(tr("Article exists. Allow duplicates if you want to re-create it"));
		break;
	case QTREADABILITY_ARTICLE_DELETED:
		qDebug() << QTREADABILITY_ARTICLE_DELETED << ": Article deleted";
		break;
	default:
		break;
	}

}


void ApplicationUI::accessToken(const QUrl &url)
{
	QString verifier = url.queryItemValue("oauth_verifier");
	qDebug() << "Verifier:" << verifier;
	_api->setVerifier(verifier);
	_api->getAccessToken();
}


void ApplicationUI::login(const QString& username, const QString& password)
{
	setLoading(true);

	_api->xAuthLogin(username.trimmed(), password.trimmed());
}


void ApplicationUI::bookmarks(const bool &refresh, const QString& page, const QString& per_page)
{
	setLoading(true);
	if (refresh)
		articles.clear();

	QtReadabilityParams filters;
	filters.insert("page", page);
	filters.insert("per_page", per_page);

	_api->getBookmarks(filters);
}


void ApplicationUI::parseBookmarks(QByteArray response) {

	const QtReadability::OAuthError &oauthError = _api->lastError();
	if (!oauthError){
		int currentPage, totalPage;
		JsonDataAccess jda;
		QVariantMap meta = jda.loadFromBuffer(response).toMap().value("meta").toMap();
		articles += jda.loadFromBuffer(response).toMap()["bookmarks"].toList(); // articles
		currentPage = meta.value("page").toInt();
		qDebug() << "CURRENT PAGE: " << currentPage;
		totalPage = meta.value("num_pages").toInt();
		qDebug() << "TOTAL PAGES: " << totalPage;
		qDebug() << "PER PAGE:" << jda.loadFromBuffer(response).toMap().value("conditions").toMap().value("per_page").toInt();
		if (currentPage != totalPage){
			qDebug() << "Another call";
			bookmarks(false, QString::number(++currentPage));
		}else{
			qDebug() << "SAVING FILE | ARTICLES COUNT: " << articles.count();
			QByteArray buffer;
			jda.saveToBuffer(articles, &buffer);
			_cacher->saveLocal(buffer);
			/*
		foreach(QVariant item, articles){
			if (!item.toMap().value("archive").toBool()){
				_model->insert(item.toMap());
			}
		}
			 */
			emit bookmarksDone();
			emit modelSizeChanged(modelSize());
			JsonDataAccess jda;
			if (!_fromAddPage && !_isInvokation && _db->cacheCount() < jda.loadFromBuffer(_cacher->getLocal()).toList().count()){
				syncContent();
			}
		}
	}else{
		if (_api->errorCode() == 500){
			internalServerError();
		}
	}
	setLoading(false);
}


void ApplicationUI::addBookmark(const QString& url, const int& favorite,
		const int& archive, const int& allow_duplicates)
{
	setLoading(true);
	_fromAddPage = true;
	qDebug() << "URL: " << url;
	_api->addBookmark(url.trimmed(), favorite, archive, allow_duplicates);
}



void ApplicationUI::parseAddBookmark(QByteArray response)
{
	Q_UNUSED(response);
	const qint32 &errorCode = _api->errorCode();
	qDebug() << "LAST ERROR:" << _api->lastError();
	qDebug() << "ERROR CODE:" << _api->errorCode();
	if (errorCode == 202){
		_api->setSynchronous(false);
		QEventLoop loop;
		loop.connect(_api, SIGNAL(responseReceived(QByteArray)), SLOT(quit()));
		bookmarks(true);
		loop.exec();
		_api->setSynchronous(true);
		if (_isInvokation){
			emit invokationMessage(tr("Article added successfully"));
		}else{
			_helper->showToast(tr("Article created successfully"));
		}
	}else if (errorCode == 409){
		QtReadabilityHeaders::const_iterator i = _api->replyHeaders().constBegin();
		while (i != _api->replyHeaders().constEnd()){
			qDebug() << i.key() << i.value();
			++i;
		}
		qDebug() << "Error code:" << _api->errorCode();
		qDebug() << "Error string:" << _api->errorString();
		if (_isInvokation){
			emit invokationMessage(tr("Article exists"));
		}else{
			_helper->showToast(tr("Article exists. Allow duplicates if you want to re-create it"));
		}
	}else if (errorCode == 400){
		if (_isInvokation){
			emit invokationMessage(tr("Bad request. Probably the url is not valid."));
		}else{
			_helper->showToast(tr("Bad request. Check the url and try again"));
		}
	}
	else if (errorCode == 500){
		if (_isInvokation){
			emit invokationMessage(tr("Internal server error. Try again later."));
		}else{
			internalServerError();
		}
	}
	_fromAddPage = false;
	setLoading(false);
}


void ApplicationUI::updateBookmark(const QString &bookmarkId,
		const int &favorite,
		const int &archive,
		const float &read_percent)
{
	setUpdating(true);

	_api->updateBookmark(bookmarkId, favorite, archive, read_percent);
}


void ApplicationUI::parseUpdateBookmark(QByteArray response)
{
	const QtReadability::OAuthError &oauthError = _api->lastError();
	if (!oauthError){
		qDebug() << "RESPONSE:" << response;
		_cacher->updateLocal(response);
		JsonDataAccess jda;
		QVariantMap temp = jda.loadFromBuffer(response).toMap();
		emit updateBookmarkDone(temp);
	}else{
		if (_api->errorCode() == 500){
			internalServerError();
		}
	}
	setUpdating(false);
}



void ApplicationUI::removeBookmark(const QString &bookmarkId)
{
	_api->deleteBookmark(bookmarkId);
}


void ApplicationUI::parseRemoveBookmark(QByteArray response)
{
	const QtReadability::OAuthError &oauthError = _api->lastError();
	if (!oauthError){
		Q_UNUSED(response)																																																QVariant article = _model->data(_indexPath);
		_cacher->removeLocal(article);
		_db->removeCache(article.toMap().value("article").toMap().value("id"));
		_model->removeAt(_indexPath);
		emit modelSizeChanged(modelSize());
		_helper->showToast(tr("Article removed successfully"));
	}else{
		if (_api->errorCode() == 500){
			internalServerError();
		}
	}
}


void ApplicationUI::article(const QString &bookmarkId)
{
	if (_syncing)
		setUpdating(true);
	_api->getArticle(bookmarkId);
}


void ApplicationUI::parseArticle(QByteArray response)
{
	const QtReadability::OAuthError &oauthError = _api->lastError();
	if (!oauthError){
		JsonDataAccess jda;
		QVariantMap article = jda.loadFromBuffer(response).toMap();
		_db->cache(article.value("id"), article.value("content"));
		if (!_syncing){
			emit articleContentDone(article.value("content"));
			_helper->showToast(tr("Content downloaded"));
		}
	}else{
		if (_api->errorCode() == 500){
			internalServerError();
		}
	}
	setUpdating(false);
}

void ApplicationUI::userInfo()
{
	setLoading(true);

	_api->getUserInfo();
}


void ApplicationUI::getShortUrl(const QString &sourceUrl)
{
	_helper->showToast(tr("Obtaining short url..."));

	_api->getShortUrl(sourceUrl);
}


void ApplicationUI::getTags()
{
	setLoading(true);

	_tagsModel->clear();

	_api->getTags();
}


void ApplicationUI::addTagsToBookmark(const QString &bookmarkId, const QString &tags)
{
	//setLoading(true);

	_api->addTagsToBookmark(bookmarkId, tags);
}

void ApplicationUI::deleteTag(const QString &tagId)
{
	//setLoading(true);

	_api->deleteTag(tagId);
}


void ApplicationUI::deleteBookmarkTag(const QString &bookmarkId, const QString &tagId)
{
	//setLoading(true);

	_api->deleteTagFromBookmark(bookmarkId, tagId);
}


void ApplicationUI::deleteSelectedTags(const QVariantList selectedTags)
{
	qDebug() << "SELECTED TAGS:" << selectedTags.count();
	_api->setSynchronous(false);
	foreach(const QVariant &indexPath, selectedTags){
		_indexPath = indexPath.toList();
		QVariant tag = _tagsModel->data(indexPath.toList()).toMap().value("id");
		QEventLoop loop;
		loop.connect(_api, SIGNAL(responseReceived(QByteArray)), SLOT(quit()));
		_api->deleteTag(tag.toString());
		loop.exec();
	}
	qDebug() << "TAGS REMOVED";
	_api->setSynchronous(true);
}


void ApplicationUI::parseUserInfo(QByteArray response)
{
	const QtReadability::OAuthError &oauthError = _api->lastError();
	if (!oauthError){
		_cacher->saveUserInfo(response);
		emit userInfoDone(_cacher->getUserInfo().toMap());
	}else{
		if (_api->errorCode() == 500){
			internalServerError();
		}
	}
	setLoading(false);
}


void ApplicationUI::parseShortUrl(QByteArray response)
{
	JsonDataAccess jda;
	QVariant shortUrl = jda.loadFromBuffer(response).toMap().value("meta").toMap().value("rdd_url");
	QVariant articleId = _model->data(_indexPath).toMap().value("article").toMap().value("id");
	QString articleTitle = _model->data(_indexPath).toMap().value("article").toMap().value("title").toString();

	qDebug() << "Article id:" << articleId.toString();
	qDebug() << "Share url:" << shortUrl;

	_helper->showToast(tr("Sharing..."));

	const QtReadability::OAuthError &error = _api->lastError();
	if (error){
		share(QString("%1\n%2 %3").arg(articleTitle).arg(_model->data(_indexPath).toMap().value("article").toMap().value("url").toString()).arg(tr("shared with #eRBook")));
	}else{
		share(QString("%1\n%2 %3").arg(articleTitle).arg(shortUrl.toString()).arg(tr("shared with #eRBook")));
		_db->setShortUrl(articleId, shortUrl);
	}

	setLoading(false);
}


void ApplicationUI::parseGetTags(QByteArray response)
{
	const QtReadability::OAuthError &oauthError = _api->lastError();
	if (!oauthError){
		qDebug() << "TAGS:" << response;
		_cacher->saveTags(response);
		JsonDataAccess jda;
		QVariantList tags = jda.loadFromBuffer(response).toMap().value("tags").toList();
		_tagsModel->insertList(tags);
		emit tagsModelChanged(tagsSize());
	}else{
		if (_api->errorCode() == 500){
			internalServerError();
		}
	}
	setLoading(false);
}


void ApplicationUI::parseSetTags(QByteArray response)
{
	qDebug() << "TAGS:" << response;

	qint32 errorCode = _api->errorCode();

	JsonDataAccess jda;
	QVariant parsedResponse = jda.loadFromBuffer(response);
	QString message = parsedResponse.toMap().value("messages").toStringList().join(", ");
	qDebug() << "ERROR CODE:" << _api->errorCode() << "MESSAGE: " << message;

	if (errorCode == 202){
		QVariantList tags = parsedResponse.toMap().value("tags").toList();
		QVariantMap bookmark = _model->data(_indexPath).toMap();
		_cacher->addTags(bookmark.value("id").toString(), tags);
		emit updateBookmarkDone(QVariantMap());
		//setLoading(false);
		_helper->showToast(tr("Bookmark was tagged successfully"));
	}else if (errorCode == 403){
		if (message.contains("No more Tags can be added to this Bookmark", Qt::CaseInsensitive)){
			_helper->showToast(tr("No more Tags can be added to this Bookmark"));
		}
		else if (message.contains("This user cannot add anymore Tags", Qt::CaseInsensitive)){
			_helper->showToast(tr("You have reached limit 500 tags"));
		}
	}else if (errorCode == 500){
		internalServerError();
	}

	if (!message.isEmpty()){
		if (message.contains("No more Tags can be added to this Bookmark", Qt::CaseInsensitive)){
			_helper->showToast(tr("No more Tags can be added to this Bookmark"));
		}
		else if (message.contains("This user cannot add anymore Tags", Qt::CaseInsensitive)){
			_helper->showToast(tr("You have reached limit 500 tags"));
		}
	}
}


void ApplicationUI::parseDeleteTag(QByteArray response)
{
	qint32 errorCode = _api->errorCode();
	if (errorCode == 204){
		qDebug() << "DELETED";
		_tagsModel->removeAt(_indexPath);
		_cacher->removeTag(_tagsModel->data(_indexPath).toMap().value("id"));
		emit tagsModelChanged(tagsSize());
		_helper->showToast(tr("Tag deleted successfully"));
	}else if (errorCode == 403){
		_helper->showToast(tr("Unable to remove this tag !"));
	}
	else if (errorCode == 500){
		internalServerError();
	}
}


void ApplicationUI::parseDeleteBookmarkTag(QByteArray response)
{
	Q_UNUSED(response);
	qint32 errorCode = _api->errorCode();
	if (errorCode == 204){
		emit bookmarkTagDeleted();
	}else if (errorCode == 500){
		internalServerError();
	}
}


QString ApplicationUI::startupMode() const {
	return m_startupMode;
}

QString ApplicationUI::source() const {
	return m_source;
}

QString ApplicationUI::target() const {
	return m_target;
}

QString ApplicationUI::action() const {
	return m_action;
}

QString ApplicationUI::mimeType() const {
	return m_mimeType;
}

QString ApplicationUI::uri() const {
	return m_uri;
}

QString ApplicationUI::data() const {
	return m_data;
}

void ApplicationUI::share(QString thedata)
{
	m_pInvocation = Invocation::create(
			InvokeQuery::create()
	.parent(this)
	.mimeType("text/plain")
	.data(thedata.toUtf8()));
	QObject::connect(m_pInvocation, SIGNAL(armed()),
			this, SLOT(onArmed()));
	QObject::connect(m_pInvocation, SIGNAL(finished()),
			m_pInvocation, SLOT(deleteLater()));
}


void ApplicationUI::onArmed()
{
	m_pInvocation->trigger("bb.action.SHARE");

}

void ApplicationUI::canceledSync()
{
	syncCanceled = true;
}

QString ApplicationUI::status() const {
	return m_status;
}

void ApplicationUI::cardDone(const QString& msg)
{
	CardDoneMessage message;
	message.setData(msg);
	message.setDataType("text/plain");
	message.setReason(tr("Success!"));

	// Send message
	_invokeManager->sendCardDone(message);
}
