/*
 * syncdialog.h
 *
 *  Created on: 9.2.2014
 *      Author: Benecore
 */

#ifndef SYNCDIALOG_H_
#define SYNCDIALOG_H_

#include <QObject>
#include <bb/system/SystemProgressDialog.hpp>

using namespace bb::system;


class SyncDialog: public QObject
{
	Q_OBJECT
public:
	SyncDialog(QObject *parent = 0);
	virtual ~SyncDialog();

	static SyncDialog *instance();


signals:
	void finished();
	void canceled();


public slots:
	void setBody(const QString &body);
	void setStatus(const QString &status);
	void setProgress(const int &progress);
	void show();
	void close();


private slots:
    void finished(bb::system::SystemUiResult::Type value);



private:
	Q_DISABLE_COPY(SyncDialog)
	static SyncDialog *_instance;
    SystemProgressDialog* _progressDialog;
};

#endif /* SYNCDIALOG_H_ */
