/*
 * syncdialog.cpp
 *
 *  Created on: 9.2.2014
 *      Author: Benecore
 */

#include "syncdialog.h"

SyncDialog *SyncDialog::_instance = 0;

SyncDialog::SyncDialog(QObject *parent) :
	QObject(parent)
{
	_progressDialog = new SystemProgressDialog(this);

	_progressDialog->setState(SystemUiProgressState::Active);
	_progressDialog->setTitle(tr("Synchronizing articles"));

	//_progressDialog->setIcon(QUrl("asset:///Images/ikona_readability.png"));

	_progressDialog->cancelButton()->setLabel(tr("Stop synchronization"));
	_progressDialog->confirmButton()->setLabel(QString::null);

	connect(_progressDialog, SIGNAL(finished(bb::system::SystemUiResult::Type)),
			this,
			SLOT(finished(bb::system::SystemUiResult::Type)));
	_progressDialog->setAutoUpdateEnabled(true);
}

SyncDialog::~SyncDialog()
{
	delete _progressDialog;
}

SyncDialog* SyncDialog::instance()
{
	if (!_instance)
		_instance = new SyncDialog();
	return _instance;
}

void SyncDialog::setBody(const QString& body)
{
	_progressDialog->setBody(body);
}

void SyncDialog::setStatus(const QString& status)
{
	_progressDialog->setStatusMessage(status);
}


void SyncDialog::setProgress(const int &progress)
{
	_progressDialog->setProgress(progress);
}


void SyncDialog::show()
{
	_progressDialog->show();
}

void SyncDialog::close()
{
	_progressDialog->setBody("");
	_progressDialog->setProgress(-1);
	_progressDialog->setStatusMessage("");
	_progressDialog->cancel();
	emit finished();
}


void SyncDialog::finished(bb::system::SystemUiResult::Type value)
{
	if (value == SystemUiResult::CancelButtonSelection){
		qDebug() << "Cancel clicked";
		emit canceled();
	}else{
	}
	emit finished();
}
