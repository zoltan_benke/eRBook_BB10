#ifndef WEBIMAGEVIEW_H_
#define WEBIMAGEVIEW_H_

#include <bb/cascades/ImageView>
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QUrl>

using namespace bb::cascades;


class WebImageView: public bb::cascades::ImageView {
	Q_OBJECT
	Q_PROPERTY (QUrl url READ url WRITE setUrl NOTIFY urlChanged)
	Q_PROPERTY (float loading READ loading NOTIFY loadingChanged)
	Q_PROPERTY(QUrl defaultImage READ defaultImage WRITE setDefaultImage NOTIFY defaultImageChanged)
public:
	WebImageView();
	const QUrl& url() const;
	const QUrl &defaultImage() const;
	double loading() const;

public Q_SLOTS:
	void setUrl(const QUrl& url);
	void setDefaultImage(const QUrl &url);
    void clearCache();

private Q_SLOTS:
	void imageLoaded();
	void dowloadProgressed(qint64,qint64);

signals:
	void urlChanged();
	void loadingChanged();
	void defaultImageChanged();


private:
	static QNetworkAccessManager * mNetManager;
	static QNetworkDiskCache * mNetworkDiskCache;
	QUrl mUrl,
	_defaultImage;
	float mLoading;

	bool isARedirectedUrl(QNetworkReply *reply);
	void setURLToRedirectedUrl(QNetworkReply *reply);
};

#endif /* WEBIMAGEVIEW_H_ */
