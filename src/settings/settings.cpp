/*
 * settings.cpp
 *
 *  Created on: 31.8.2013
 *      Author: Benecore
 */

#include "settings.h"


Settings *Settings::_instance = NULL;

Settings::Settings(QObject *parent) :
	QObject(parent)
{
	// TODO Auto-generated constructor stub
	settings = new QSettings("DevPDA", "eRBook");

	/* Load default settings */
#ifdef TESTERS
	_isPro = settings->value("isPro", true).toBool();
#else
	_isPro = settings->value("isPro", false).toBool();
#endif
	_listLayout = settings->value("listLayout", true).toBool();
	_sortAscending = settings->value("sortAscending", false).toBool();
	_activeTab = settings->value("activeTab", 0).toInt();
	_syncStartup = settings->value("syncStartup", true).toBool();
	_loadImages = settings->value("loadImages", true).toBool();
	_grouping = settings->value("grouping", false).toBool();
	_activeColor = settings->value("activeColor", "0098f0");
	_fontSize = settings->value("fontSize", 32).toInt();
	_backColor = settings->value("backColor", "#f8f8f8");
	_nightMode = settings->value("nightMode", false).toBool();
	_fullScreen = settings->value("fullScreen", false).toBool();
	_sortingKeys = settings->value("sortingKeys", "date_added");
	_readMode = settings->value("readMode", 0).toInt();
	_readFamily = settings->value("readFamily", 0).toInt();
	_useShortener = settings->value("useShortener", false).toBool();
	_whiteHeader = settings->value("whiteHeader", false).toBool();
	_syncAfterAdd = settings->value("syncAfterAdd", false).toBool();
	_tagsGrouping = settings->value("tagsGrouping", true).toBool();
	_tagsSortAscending = settings->value("tagsSortAscending", true).toBool();
	_closeInvoke = settings->value("closeInvoke", true).toBool();
}

Settings::~Settings()
{
	delete settings;
}


Settings *Settings::instance()
{
	if (!_instance)
		_instance = new Settings;
	return _instance;
}


void Settings::destroy()
{
	if (_instance){
		delete _instance;
		_instance = NULL;
	}
}
