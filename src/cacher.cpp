/*
 * cacher.cpp
 *
 *  Created on: 20.1.2014
 *      Author: Benecore
 */

#include "cacher.h"

//#ifdef __arm__
const QString LOCAL_FILE = "data/local.json";
const QString REMOTE_FILE = "data/remote.json";
const QString USER_FILE = "data/user.json";
const QString TAGS_FILE = "data/tags.json";
//#else
//const QString LOCAL_FILE = "app/native/assets/local.json";
//const QString REMOTE_FILE = "app/native/assets/remote.json";
//const QString USER_FILE = "app/native/assets/user.json";
//#endif

Cacher *Cacher::_instance = 0;

Cacher::Cacher(QObject *parent) :
										QObject(parent)
{
	_local = new QFile(LOCAL_FILE);
	_remote = new QFile(REMOTE_FILE);
	_user = new QFile(USER_FILE);
	_tags = new QFile(TAGS_FILE);
}

Cacher::~Cacher()
{
	delete _local;
	delete _remote;
	delete _user;
	delete _tags;
}


Cacher* Cacher::instance()
{
	if (!_instance)
		_instance = new Cacher;
	return _instance;
}


void Cacher::saveLocal(const QByteArray& data)
{
	if (!_local->open(QIODevice::WriteOnly)){
		qWarning() << "Unable to open local file: " << _local->errorString();
		return;
	}
	_local->write(data);
	_local->flush();
	_local->close();
}


void Cacher::updateLocal(const QByteArray &data)
{
	QByteArray temp = getLocal();
	JsonDataAccess jda;
	QVariantMap newItem = jda.loadFromBuffer(data).toMap();
	QVariantList list = jda.loadFromBuffer(temp).toList();
	int replaceIndex;
	for (int i = 0; i < list.count(); ++i){
		QVariantMap item = list.at(i).toMap();
		if (item.value("id") == newItem.value("id")){
			replaceIndex = i;
			break;
		}
	}
	list.replace(replaceIndex, newItem);
	temp.clear();
	jda.saveToBuffer(list, &temp);
	saveLocal(temp);
}


void Cacher::addTags(const QString &id, const QVariantList &tags)
{
	QByteArray temp = getLocal();
	JsonDataAccess jda;
	QVariantMap item;
	QVariantList list = jda.loadFromBuffer(temp).toList();
	int replaceIndex;
	for (int i = 0; i < list.count(); ++i){
		item = list.at(i).toMap();
		if (item.value("id").toString() == id){
			QVariantList currentTags = item.value("tags").toList();
			currentTags += tags;
			item.insert("tags", currentTags);
			replaceIndex = i;
			break;
		}
	}
	list.replace(replaceIndex, item);
	temp.clear();
	jda.saveToBuffer(list, &temp);
	saveLocal(temp);
}


void Cacher::removeLocal(const QVariant &data)
{
	int removeIndex;
	QByteArray temp = getLocal();
	JsonDataAccess jda;
	QVariantMap removeItem = data.toMap();
	QVariantList list = jda.loadFromBuffer(temp).toList();
	for (int i = 0; i < list.count(); ++i){
		QVariantMap item = list.at(i).toMap();
		if (item.value("id") == removeItem.value("id")){
			removeIndex = i;
			break;
		}
	}
	list.removeAt(removeIndex);
	temp.clear();
	jda.saveToBuffer(list, &temp);
	saveLocal(temp);
}


QByteArray Cacher::getLocal()
{
	if (!_local->open(QIODevice::ReadOnly)){
		qWarning() << "Unable to open local file: " << _local->errorString();
		return QByteArray();
	}
	QByteArray data = _local->readAll();
	_local->close();
	return data;
}


int Cacher::getTotalArticles()
{
	QByteArray local = getLocal();
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(local).toList();
	return list.count();
}

void Cacher::saveRemote(const QByteArray& data)
{
	if (!_remote->open(QIODevice::WriteOnly)){
		qWarning() << "Unable to open remote file: " << _remote->errorString();
		return;
	}
	_remote->write(data);
	_remote->flush();
	_remote->close();
}

QByteArray Cacher::getRemote()
{
	if (!_remote->open(QIODevice::ReadOnly)){
		qWarning() << "Unable to open local file: " << _remote->errorString();
		return QByteArray();
	}
	QByteArray data = _remote->readAll();
	_remote->close();
	return data;
}



QList<QVariant> Cacher::compare()
{
	JsonDataAccess jda;
	QVariantList remote = jda.loadFromBuffer(getRemote()).toList();
	QVariantList local = jda.loadFromBuffer(getLocal()).toList();
}

void Cacher::saveUserInfo(const QByteArray& data)
{
	if (!_user->open(QIODevice::WriteOnly)){
		qWarning() << "Unable to open remote file: " << _user->errorString();
		return;
	}
	_user->write(data);
	_user->flush();
	_user->close();
}

void Cacher::saveTags(const QByteArray& data)
{
	if (!_tags->open(QIODevice::WriteOnly)){
		qWarning() << "Unable to open local file: " << _tags->errorString();
		return;
	}
	_tags->write(data);
	_tags->flush();
	_tags->close();
}

QByteArray Cacher::getTags()
{
	if (!_tags->open(QIODevice::ReadOnly)){
		qWarning() << "Unable to open local file: " << _tags->errorString();
		return QByteArray();
	}
	QByteArray data = _tags->readAll();
	_tags->close();
	return data;
}


QVariantList Cacher::getBookmarkTags(const QVariant &bookmarkId)
{
	int selectedBookmark;
	QByteArray temp = getLocal();
	JsonDataAccess jda;
	QVariantList bookmarks = jda.loadFromBuffer(temp).toList();
	for (int i = 0; i < bookmarks.count(); ++i){
		QVariantMap bookmark = bookmarks.at(i).toMap();
		if (bookmark.value("id") == bookmarkId){
			selectedBookmark = i;
			break;
		}
	}
	QVariantList tags = bookmarks.at(selectedBookmark).toMap().value("tags").toList();
	return tags;
}


void Cacher::removeTag(const QVariant &id)
{
	int removeIndex;
	QByteArray temp = getTags();
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(temp).toList();
	for (int i = 0; i < list.count(); ++i){
		QVariantMap item = list.at(i).toMap();
		if (item.value("id") == id){
			removeIndex = i;
			break;
		}
	}
	list.removeAt(removeIndex);
	temp.clear();
	jda.saveToBuffer(list, &temp);
	saveTags(temp);
}


void Cacher::removeBookmarkTag(const QVariant &bookmarkId, const QVariant &tagId)
{
	int removeIndex;
	QByteArray tempBookmarks = getLocal();
	QVariantList bookmarkTags;
	QVariantMap bookmark;
	JsonDataAccess jda;
	QVariantList list = jda.loadFromBuffer(tempBookmarks).toList();
	for (int i = 0; i < list.count(); ++i){
		bookmark = list.at(i).toMap();
		if (bookmark.value("id") == bookmarkId){
			bookmarkTags = bookmark.value("tags").toList();
			break;
		}
	}
	for (int i = 0; i < bookmarkTags.count(); ++i){
		QVariantMap tag = bookmarkTags.at(i).toMap();
		if (tag.value("id") == tagId){
			removeIndex = i;
			break;
		}
	}
	bookmarkTags.removeAt(removeIndex);

	bookmark.insert("tags", bookmarkTags);
	QByteArray data;
	jda.saveToBuffer(bookmark, &data);
	updateLocal(data);
}


QVariant Cacher::getUserInfo()
{
	if (!_user->open(QIODevice::ReadOnly)){
		qWarning() << "Unable to open local file: " << _user->errorString();
		return QVariant();
	}
	QByteArray data = _user->readAll();
	_user->close();
	JsonDataAccess jda;
	QVariant user = jda.loadFromBuffer(data);
	return user;
}
