#include "helper.h"


HelperUtils *HelperUtils::_instance = NULL;

HelperUtils::HelperUtils(QObject *parent) :
				QObject(parent), dialog(0), toast(0)
{
	toast = new SystemToast(this);
}


HelperUtils::~HelperUtils()
{
	delete dialog;
	delete toast;
}



void HelperUtils::dialogFinished(bb::system::SystemUiResult::Type result)
{
	switch(result){
	case SystemUiResult::ConfirmButtonSelection:
		emit okClicked();
		break;
	default:
		break;
	}
	dialog->deleteLater();
}


HelperUtils *HelperUtils::instance()
{
	if (!_instance)
		_instance = new HelperUtils;
	return _instance;
}


void HelperUtils::destroy()
{
	if (_instance){
		delete _instance;
		_instance = NULL;
	}
}


QString HelperUtils::formatText(const QString &text)
{
	QTextDocument doc;
	doc.setHtml(text);
	return doc.toPlainText().trimmed();
}



QString HelperUtils::removeHref(QString content)
{
	QString temp = content.replace(QRegExp("href="), "");
	return temp;
}

void HelperUtils::showDialog(const QString &confirmLabel, const QString &cancelLabel, const QString &title, const QString &message)
{
	dialog = new SystemDialog(confirmLabel, cancelLabel);
	this->ok = ok;
	dialog->setTitle(title);
	dialog->setBody(message);
	bool success = connect(dialog, SIGNAL(finished(bb::system::SystemUiResult::Type)), this,
			SLOT(dialogFinished(bb::system::SystemUiResult::Type)));
	if (success){
		dialog->show();
	}else{
		dialog->deleteLater();
	}
}


void HelperUtils::showToast(const QString& message)
{
	toast->setAutoUpdateEnabled(true);
	toast->setPosition(SystemUiPosition::MiddleCenter);
	toast->setBody(message);
	toast->show();
}



bool HelperUtils::validUrl(QString url)
{
	QRegExp validURLRegex("^(http|https)://[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(([0-9]{1,5})?/?.*)$");
	QRegExpValidator validator(validURLRegex);
	int index = 0;
	if (validator.validate(url, index) == QValidator::Acceptable) {
		return true;
	}
	return false;
}
