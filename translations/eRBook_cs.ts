<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../assets/AboutPage.qml" line="12"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Zavřít</translation>
    </message>
    <message>
        <source>Thank you for your support</source>
        <translation type="obsolete">Děkujeme za podporu</translation>
    </message>
    <message>
        <source>eRBook</source>
        <translation type="obsolete">eRBook</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="31"/>
        <source>Thank you for your support. Now you have a PRO version. Enjoy ;)</source>
        <translation>Děkuji vám za vaši podporu. Nyní máte PRO verzi. Užijte si;)</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="42"/>
        <source>No existing purchased</source>
        <translation>Žádné stávající zakoupení</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="45"/>
        <source>Purchase restored. Thank you</source>
        <translation>Nákup obnoven. Děkuji</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="80"/>
        <source>eRBook %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="91"/>
        <source>Version: %1</source>
        <translation>Verze: %1</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="101"/>
        <source>Developer: %1</source>
        <translation>Vývojář: %1</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="119"/>
        <source>In case of any questions or ideas, please contact me:</source>
        <translation>V případě jakýchkoliv dotazů nebo nápadů, prosím, kontaktujte mě:</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="164"/>
        <source>This is a FREE version of eRBook. Free version has a three limitations

&lt;i&gt;- Can&apos;t add/remove tags of articles&lt;/i&gt;
&lt;i&gt;- Can&apos;t change Style of reading&lt;/i&gt;
&lt;i&gt;- Can&apos;t change font&lt;/i&gt;</source>
        <translation>Tohle je bezplatná verze eRBook. Bezplatná verze má tři omezení

&lt;i&gt;- Nelze přidat/dstranit značky článku&lt;/i&gt;
&lt;i&gt;- Nelze zmenit styl čtení&lt;/i&gt;
&lt;i&gt;- Nelze měnit písmo&lt;/i&gt;</translation>
    </message>
    <message>
        <source>This is a FREE version of eRBook. Free version has a three limitations

&lt;i&gt;- Can&apos;t add a new article&lt;/i&gt;
&lt;i&gt;- Can&apos;t change Day/Night mode&lt;/i&gt;
&lt;i&gt;- Can&apos;t change font size&lt;/i&gt;</source>
        <translation type="obsolete">Tohle je bezplatná verze eRBook. Bezplatná verze má tři omezení

&lt;i&gt;- Nelze přidat nový článek&lt;/i&gt;
&lt;i&gt;- Nelze měnit denní / noční režim&lt;/i&gt;
&lt;i&gt;- Nelze měnit velikost písma&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="177"/>
        <source>Buy PRO version</source>
        <translation>Koupit PRO verzi</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="186"/>
        <source>Already purchased</source>
        <translation>Již zakoupené</translation>
    </message>
    <message>
        <source>Version: 1.0.0.2</source>
        <translation type="obsolete">Verze: 1.0.0.2</translation>
    </message>
    <message>
        <source>Version: 1.0.0.1</source>
        <translation type="obsolete">Verze: 1.0.0.1</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="112"/>
        <source>A fully featured Readability client that allows you to take your reading list.</source>
        <translation>Readability.com klient, který vám umožní mít své oblíbené články vždy se sebou.</translation>
    </message>
    <message>
        <source>In case of any questions or ideas, please contact me at &lt;span style=&quot;color:#a30001&quot;&gt;support@devpda.net&lt;/span&gt;</source>
        <translation type="obsolete">V případě jakýchkoliv dotazů nebo nápadů, prosím, kontaktujte mě na &lt;span style=&quot;color:#a30001&quot;&gt;support@devpda.net&lt;/span&gt;</translation>
    </message>
    <message>
        <source>Support development</source>
        <translation type="obsolete">Podpoř vývoj aplikace</translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="148"/>
        <source>Copyright (c) 2013 DevPDA</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AddPage</name>
    <message>
        <source>Article already exists. Use &quot;Allow duplicates&quot; if you want to overwrite article</source>
        <translation type="obsolete">Článek již existuje. Použijte &quot;Povolit duplicitní&quot;, pokud chcete přepsat článek</translation>
    </message>
    <message>
        <source>Enter a valid Url</source>
        <translation type="obsolete">Zadejte platnou adresu URL</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="27"/>
        <location filename="../assets/AddPage.qml" line="186"/>
        <source>Add article</source>
        <translation>Přidat článek</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="30"/>
        <source>Adding article...</source>
        <translation>Přidávám článek...</translation>
    </message>
    <message>
        <source>Article URL</source>
        <translation type="obsolete">Url</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="65"/>
        <location filename="../assets/Invocation/AddPage.qml" line="65"/>
        <source>url</source>
        <translation></translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Přilepit</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Odstranit</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="12"/>
        <source>You must enter a valid url</source>
        <translation>Musíte zadat platnou adresu URL</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="40"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <source>Buy PRO</source>
        <translation type="obsolete">Koupit PRO</translation>
    </message>
    <message>
        <source>You can&apos;t add article because you have a FREE version of eRBook</source>
        <translation type="obsolete">Nemůžete přidat článek, protože použiváte bezplatnou verzi eRBook</translation>
    </message>
    <message>
        <source>Buy</source>
        <translation type="obsolete">Koupit</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="75"/>
        <source>Invalid URL</source>
        <translation>Neplatná adresa URL</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="97"/>
        <source>Archive</source>
        <translation>Archivovat</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="126"/>
        <source>Favorite</source>
        <translation>Oblíbený</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="155"/>
        <source>Allow duplicates *</source>
        <translation>Povolit duplicitní *</translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="179"/>
        <source>* Allow duplicates automatically rewrite existing article</source>
        <translation>* Povolit duplicitní automaticky přepíše existující článek</translation>
    </message>
    <message>
        <location filename="../assets/Invocation/AddPage.qml" line="22"/>
        <source>Sharing article</source>
        <translation>Sdílení článku</translation>
    </message>
    <message>
        <location filename="../assets/Invocation/AddPage.qml" line="23"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <source>Can&apos;t add new article in free version of eRBook</source>
        <translation type="obsolete">Nelze přidat nový článek v bezplatné verzi eRBook</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Zrušit</translation>
    </message>
    <message>
        <source>Ok, maybe later ;)</source>
        <translation type="obsolete">Ok, možná později ;)</translation>
    </message>
    <message>
        <source>Share article</source>
        <translation type="obsolete">Sdílet článek</translation>
    </message>
</context>
<context>
    <name>ApplicationUI</name>
    <message>
        <location filename="../src/applicationui.cpp" line="131"/>
        <source>Launch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="136"/>
        <source>Invoke</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="141"/>
        <source>Card</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="278"/>
        <source>Resized</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="285"/>
        <source>Pooled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="286"/>
        <source>--</source>
        <translation></translation>
    </message>
    <message>
        <source>Contents of all articles is already available</source>
        <translation type="obsolete">Obsah všech článků je již k dispozici</translation>
    </message>
    <message>
        <source>All contents downloaded successfully</source>
        <translation type="obsolete">Veškerý obsah byl úspěšně stažen</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="498"/>
        <source>Synchronization canceled. Some of the articles was not successfully synced</source>
        <translation>Synchronizace zrušena. Některé články nebyly synchronizovány</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="594"/>
        <source>Bad request. Check your URL</source>
        <translation>Špatný požadavek. Zkontrolujte URL</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="598"/>
        <source>Invalid login. Check you credentials and try again</source>
        <translation>Neplatné přihlášení. Zkontrolujte vaše přihlašovací údaje a zkuste to znovu</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="608"/>
        <source>Internal server error. Try again later</source>
        <translation>Vnitřní chyba serveru. Zkuste to znovu později</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="480"/>
        <source>Starting synchronization</source>
        <translation>Spuštění synchronizace</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="486"/>
        <source>Syncing article</source>
        <translation>Synchronizace článku</translation>
    </message>
    <message>
        <source>Article shared</source>
        <translation type="obsolete">Článek sdílen</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="732"/>
        <source>Article created successfully</source>
        <translation>Článek byl úspěšně vytvořen</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="743"/>
        <source>Article exists</source>
        <translation>Článek existuje</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="751"/>
        <source>Bad request. Check the url and try again</source>
        <translation>Špatný požadavek. Zkontrolujte adresu URL a zkuste to znovu</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="756"/>
        <source>Internal server error. Try again later.</source>
        <translation>Vnitřní chyba serveru. Zkuste to znovu později.</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="811"/>
        <source>Article removed successfully</source>
        <translation>Článek byl úspěšně odstraněn</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="837"/>
        <source>Content downloaded</source>
        <translation>Obsah stažen</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="857"/>
        <source>Obtaining short url...</source>
        <translation>Získávání zkrácené url...</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="938"/>
        <source>Sharing...</source>
        <translation>Sdílení...</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="942"/>
        <location filename="../src/applicationui.cpp" line="944"/>
        <source>shared with #eRBook</source>
        <translation>sdílené přes #eRBook</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="991"/>
        <location filename="../src/applicationui.cpp" line="1002"/>
        <source>No more Tags can be added to this Bookmark</source>
        <translation>Článek dosáhl maximální počet značek</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="994"/>
        <location filename="../src/applicationui.cpp" line="1005"/>
        <source>You have reached limit 500 tags</source>
        <translation>Dosáhli jste limitu: 500 značek</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="988"/>
        <source>Bookmark was tagged successfully</source>
        <translation>Článek byl úspěšně označen</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="730"/>
        <source>Article added successfully</source>
        <translation>Článek byl úspěšně přidán</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="749"/>
        <source>Bad request. Probably the url is not valid.</source>
        <translation>Špatný požadavek. Pravděpodobně není platná url.</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="1019"/>
        <source>Tag deleted successfully</source>
        <translation>Značka úspěšně smazána</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="1021"/>
        <source>Unable to remove this tag !</source>
        <translation>Nelze odstranit tuto značku!</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="1103"/>
        <source>Success!</source>
        <translation>Úspěšné!</translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="618"/>
        <location filename="../src/applicationui.cpp" line="745"/>
        <source>Article exists. Allow duplicates if you want to re-create it</source>
        <translation>Článek již existuje. Použijte &quot;Povolit duplicitní&quot;, pokud chcete přepsat článek</translation>
    </message>
    <message>
        <location filename="../src/applicationui.hpp" line="313"/>
        <source>Internal server error. Try again later !</source>
        <translation>Vnitřní chyba serveru. Zkuste to znovu později!</translation>
    </message>
</context>
<context>
    <name>Archives</name>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="35"/>
        <source>Downloading content... %1/%2</source>
        <translation>Stahování obsahu... %1/%2</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="54"/>
        <source>Archives</source>
        <translation>Archiv</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="66"/>
        <source>Add article</source>
        <translation>Přidat článek</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="74"/>
        <source>GridView</source>
        <translation>Mřížka</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="74"/>
        <source>ListView</source>
        <translation>List</translation>
    </message>
    <message>
        <source>Re-sync articles</source>
        <translation type="obsolete">Znovu synchronizovat</translation>
    </message>
    <message>
        <source>Re-sync articles...</source>
        <translation type="obsolete">Synchronizuji...</translation>
    </message>
    <message>
        <source>Download contents of articles</source>
        <translation type="obsolete">Stáhnout obsah článků</translation>
    </message>
    <message>
        <source>No articles</source>
        <translation type="obsolete">Žádné články</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Neznámý</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="60"/>
        <source>No archived articles</source>
        <translation>Žádné archivované články</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Obnovit</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="93"/>
        <source>Sync</source>
        <translation>Synchronizace</translation>
    </message>
</context>
<context>
    <name>CustomDialog</name>
    <message>
        <source>Day</source>
        <translation type="obsolete">Den</translation>
    </message>
    <message>
        <source>Night</source>
        <translation type="obsolete">Noc</translation>
    </message>
    <message>
        <source>Sepia</source>
        <translation type="obsolete">Sépie</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="36"/>
        <source>Downloading content... %1/%2</source>
        <translation>Stahování obsahu... %1/%2</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="55"/>
        <source>Favorites</source>
        <translation>Oblíbené</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="67"/>
        <source>Add article</source>
        <translation>Přidat článek</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="75"/>
        <source>GridView</source>
        <translation>Mřížka</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="75"/>
        <source>ListView</source>
        <translation>List</translation>
    </message>
    <message>
        <source>Re-sync articles</source>
        <translation type="obsolete">Znovu synchronizovat</translation>
    </message>
    <message>
        <source>Re-sync articles...</source>
        <translation type="obsolete">Synchronizuji...</translation>
    </message>
    <message>
        <source>Download contents of articles</source>
        <translation type="obsolete">Stáhnout obsah článků</translation>
    </message>
    <message>
        <source>No articles</source>
        <translation type="obsolete">Žádné články</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Neznámý</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="61"/>
        <source>No favorites articles</source>
        <translation>Žádné oblíbené články</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Obnovit</translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="94"/>
        <source>Sync</source>
        <translation>Synchronizace</translation>
    </message>
</context>
<context>
    <name>HelpLogin</name>
    <message>
        <source>Help</source>
        <translation type="obsolete">Nápověda</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Zavřít</translation>
    </message>
    <message>
        <source>eRBook</source>
        <translation type="obsolete">eRBook</translation>
    </message>
    <message>
        <source>is fully-featured Readability.com client. For the use you need a Readability.com account credentials.</source>
        <translation type="obsolete">je plně vybavený Readability.com klient. Pro použití budete potřebovat Readability.com účet.</translation>
    </message>
    <message>
        <source>Privacy Policy</source>
        <translation type="obsolete">Privacy Policy</translation>
    </message>
    <message>
        <source>eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook</source>
        <translation type="obsolete">eRBook ukladá pouze přístupový token. Přístupový token je uložen lokálně ve Vašem telefonu a je odstraněn při odinstalaci aplikace</translation>
    </message>
</context>
<context>
    <name>ImageButtonText</name>
    <message>
        <location filename="../assets/Components/ImageButtonText.qml" line="16"/>
        <source>Test</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LeadingVisual</name>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="7"/>
        <source>Sorting by</source>
        <translation>Třídit podle</translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="11"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="15"/>
        <source>Title</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="29"/>
        <source>Sort</source>
        <translation>Třídit</translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="33"/>
        <source>Ascending</source>
        <translation>Vzestupně</translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="37"/>
        <source>Descending</source>
        <translation>Sestupně</translation>
    </message>
</context>
<context>
    <name>ListItemArticles</name>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="41"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="44"/>
        <source>Context menu</source>
        <translation>Kontextové menu</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="45"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="48"/>
        <source>Unarchive article</source>
        <translation>Označit jako nepřečtený</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="45"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="48"/>
        <source>Archive article</source>
        <translation>Označit jako přečtený</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="52"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="55"/>
        <source>Unfavorite article</source>
        <translation>Odstranit z oblíbených</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="52"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="55"/>
        <source>Favorite article</source>
        <translation>Přidat k oblíbeným</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="59"/>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="82"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="62"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="85"/>
        <source>Tag it</source>
        <translation>Označit článek</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="66"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="69"/>
        <source>Can&apos;t tag articles in free version</source>
        <translation>Nelze označit články v bezplatné verzi</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="72"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="75"/>
        <source>Tag bookmark</source>
        <translation>Označit článek</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="90"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="93"/>
        <source>Share</source>
        <translation>Sdílet</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="118"/>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="126"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="121"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="129"/>
        <source>Remove article</source>
        <translation>Odstranit článek</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="127"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="130"/>
        <source>Are you sure you want to remove this article and its content?</source>
        <translation>Jste si jisti, že chcete odstranit tento článek a jeho obsah?</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="129"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="132"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="108"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="111"/>
        <source>Open in browser</source>
        <translation>Otevřít v prohlížeči</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="101"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="104"/>
        <source>Sharing...</source>
        <translation>Sdílení...</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="98"/>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="102"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="101"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="105"/>
        <source> shared with #eRBook</source>
        <translation> sdílené přes #eRBook</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="75"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="78"/>
        <source>comma separated tags</source>
        <translation>značky oddělené čárkami</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="84"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="87"/>
        <source>Text will be converted to lowercase automatically</source>
        <translation>Text bude automaticky převeden na malá písmena</translation>
    </message>
</context>
<context>
    <name>ListItemArticlesGrid</name>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="42"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="45"/>
        <source>Context menu</source>
        <translation>Kontextové menu</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="46"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="49"/>
        <source>Unarchive article</source>
        <translation>Označit jako nepřečtený</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="46"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="49"/>
        <source>Archive article</source>
        <translation>Označit jako přečtený</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="53"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="56"/>
        <source>Unfavorite article</source>
        <translation>Odstranit z oblíbených</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="53"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="56"/>
        <source>Favorite article</source>
        <translation>Přidat k oblíbeným</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="60"/>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="83"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="63"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="86"/>
        <source>Tag it</source>
        <translation>Označit článek</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="67"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="70"/>
        <source>Can&apos;t tag articles in free version</source>
        <translation>Nelze označit články v bezplatné verzi</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="73"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="76"/>
        <source>Tag bookmark</source>
        <translation>Označit článek</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="91"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="94"/>
        <source>Share</source>
        <translation>Sdílet</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="119"/>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="127"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="122"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="130"/>
        <source>Remove article</source>
        <translation>Odstranit článek</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="128"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="131"/>
        <source>Are you sure you want to remove this article and its content?</source>
        <translation>Jste si jisti, že chcete odstranit tento článek a jeho obsah?</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="130"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="133"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="109"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="112"/>
        <source>Open in browser</source>
        <translation>Otevřít v prohlížeči</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="102"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="105"/>
        <source>Sharing...</source>
        <translation>Sdílení...</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="99"/>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="103"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="102"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="106"/>
        <source> shared with #eRBook</source>
        <translation> sdílené přes #eRBook</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="76"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="79"/>
        <source>comma separated tags</source>
        <translation>značky oddělené čárkami</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="85"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="88"/>
        <source>Text will be converted to lowercase automatically</source>
        <translation>Text bude automaticky převeden na malá písmena</translation>
    </message>
</context>
<context>
    <name>ListItemBookmarkTags</name>
    <message>
        <location filename="../assets/Components/ListItemBookmarkTags.qml" line="62"/>
        <source>Can&apos;t remove tags in free version of app</source>
        <translation>Nelze odebrat tagy v bezplatné verzi</translation>
    </message>
    <message>
        <location filename="../assets/Components/ListItemBookmarkTags.qml" line="69"/>
        <source>Remove &apos;%1&apos; tag</source>
        <translation>Odstranit &apos;%1&apos; značku</translation>
    </message>
    <message>
        <location filename="../assets/Components/ListItemBookmarkTags.qml" line="70"/>
        <source>Are you sure you want to remove this tag?</source>
        <translation>Jste si jisti, že chcete odstranit tuto značku?</translation>
    </message>
    <message>
        <location filename="../assets/Components/ListItemBookmarkTags.qml" line="72"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
</context>
<context>
    <name>ListItemDelegate</name>
    <message>
        <source>Select</source>
        <translation type="obsolete">Vybrat</translation>
    </message>
    <message>
        <source>Unarchive this article</source>
        <translation type="obsolete">Označit jako nepřečtený</translation>
    </message>
    <message>
        <source>Archive this article</source>
        <translation type="obsolete">Označit jako přečtený</translation>
    </message>
    <message>
        <source>Remove from Favorites</source>
        <translation type="obsolete">Odstranit z oblíbených</translation>
    </message>
    <message>
        <source>Favorite this article</source>
        <translation type="obsolete">Přidat k oblíbeným</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Odstranit</translation>
    </message>
    <message>
        <source>Delete?</source>
        <translation type="obsolete">Odstranit?</translation>
    </message>
    <message>
        <source>Are you sure you want to permanently delete this article?</source>
        <translation type="obsolete">Jste si jisti, že chcete natrvalo odstranit tento článek?</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Odstranit</translation>
    </message>
</context>
<context>
    <name>ListItemTags</name>
    <message>
        <location filename="../assets/720x720/Components/ListItemTags.qml" line="12"/>
        <location filename="../assets/Components/ListItemTags.qml" line="12"/>
        <source>Remove tag</source>
        <translation>Odstranit značku</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemTags.qml" line="20"/>
        <location filename="../assets/Components/ListItemTags.qml" line="24"/>
        <source>Remove &apos;%1&apos; tag</source>
        <translation>Odstranit &apos;%1&apos; značku</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemTags.qml" line="21"/>
        <location filename="../assets/Components/ListItemTags.qml" line="25"/>
        <source>Are you sure you want to remove this tag?</source>
        <translation>Jste si jisti, že chcete odstranit tuto značku?</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemTags.qml" line="23"/>
        <location filename="../assets/Components/ListItemTags.qml" line="27"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../assets/Components/ListItemTags.qml" line="18"/>
        <source>Can&apos;t remove tags in free version of app</source>
        <translation>Nelze odebrat tagy v bezplatné verzi</translation>
    </message>
</context>
<context>
    <name>LoadingIndicator</name>
    <message>
        <location filename="../assets/Common/LoadingIndicator.qml" line="24"/>
        <source>Loading articles...</source>
        <translation>Načítání článků...</translation>
    </message>
    <message>
        <location filename="../assets/Common/LoadingIndicator.qml" line="27"/>
        <source>Downloading content... 0/0</source>
        <translation>Stahování obsahu... 0/0</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Invalid credentials. Check your login information and try again.</source>
        <translation type="obsolete">Neplatná přihlašovací údaje. Zkontrolujte je a zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="49"/>
        <source>Welcome</source>
        <translation>Vítejte</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Nápověda</translation>
    </message>
    <message>
        <source>Logging in...</source>
        <translation type="obsolete">Přihlašuji se...</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="16"/>
        <source>Please enter your credentials to continue</source>
        <translation>Pro pokračování zadejte své přihlašovací údaje</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="52"/>
        <source>Signing in...</source>
        <translation>Přihlašování...</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="71"/>
        <source>Please login</source>
        <translation>Prosím přihlašte se</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="82"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="94"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="112"/>
        <source>Login</source>
        <translation>Přihlášení</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="118"/>
        <source>Register</source>
        <translation>Registrace</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="137"/>
        <source>If you having trouble with classic login try to login via web page</source>
        <translation>Máte-li potíže s klasickým přihlášením zkuste se přihlásit přes webové stránky</translation>
    </message>
    <message>
        <source>If you having trouble with classic login try to login via Web page</source>
        <translation type="obsolete">Máte-li potíže s klasickým přihlášením zkuste se přihlásit přes webové stránky</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="150"/>
        <source>Web login</source>
        <translation>Web přihlášení</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="161"/>
        <source>Privacy policy</source>
        <translation>Ochrana osobních údajů</translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="172"/>
        <source>eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook</source>
        <translation>eRBook ukladá pouze přístupový token. Přístupový token je uložen lokálně ve Vašem telefonu a je odstraněn při odinstalaci aplikace</translation>
    </message>
</context>
<context>
    <name>LoginPageWeb</name>
    <message>
        <location filename="../assets/LoginPageWeb.qml" line="41"/>
        <source>Web login</source>
        <translation>Web přihlášení</translation>
    </message>
    <message>
        <location filename="../assets/LoginPageWeb.qml" line="42"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../assets/LoginPageWeb.qml" line="42"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
</context>
<context>
    <name>MiddleIndicator</name>
    <message>
        <source>Please wait...</source>
        <translation type="obsolete">Čekejte prosím...</translation>
    </message>
</context>
<context>
    <name>MyIndicator</name>
    <message>
        <source>Syncing articles...</source>
        <translation type="obsolete">Synchronizuji...</translation>
    </message>
    <message>
        <source>Loading articles...</source>
        <translation type="obsolete">Načítání...</translation>
    </message>
</context>
<context>
    <name>ReadPage</name>
    <message>
        <source>This article is not yet parsed succesfully (server side). Try Re-sync article.</source>
        <translation type="obsolete">Tento článek ještě nebyl analyzován úspěšně (na straně serveru). Zkuste znovu synchronizovat článek.</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="162"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <source>Font size</source>
        <translation type="obsolete">Velikost písma</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="189"/>
        <source>Can not change font in free version</source>
        <translation>Nelze změnit písmo v bezplatné verzi</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="200"/>
        <source>Sepia mode</source>
        <translation>Sepia režim</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="228"/>
        <location filename="../assets/ReadPage.qml" line="250"/>
        <source>Tag it</source>
        <translation>Označit článek</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="234"/>
        <source>Can&apos;t tag articles in free version</source>
        <translation>Nelze označit články v bezplatné verzi</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="240"/>
        <source>Tag bookmark</source>
        <translation>Označit článek</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="243"/>
        <source>comma separated tags</source>
        <translation>značky oddělené čárkami</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="252"/>
        <source>Text will be converted to lowercase automatically</source>
        <translation>Text bude automaticky převeden na malá písmena</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="257"/>
        <source>Show tags</source>
        <translation>Zobrazit značky</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="265"/>
        <source>No tags for this bookmark</source>
        <translation>Žádné značky pro tento článek</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="311"/>
        <source>Unarchive article</source>
        <translation>Označit jako nepřečtený</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="311"/>
        <source>Archive article</source>
        <translation>Označit jako přečtený</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="319"/>
        <source>Unfavorite article</source>
        <translation>Odstranit z oblíbených</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="319"/>
        <source>Favorite article</source>
        <translation>Přidat k oblíbeným</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="300"/>
        <source>Open in browser</source>
        <translation>Otevřít v prohlížeči</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="327"/>
        <source>Share</source>
        <translation>Sdílet</translation>
    </message>
    <message>
        <source>Unarchive this article</source>
        <translation type="obsolete">Označit jako nepřečtený</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="15"/>
        <source>Article favorited</source>
        <translation>Článek přidán k oblíbeným</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="18"/>
        <source>Article unfavorited</source>
        <translation>Článek odstraněn z oblíbených</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="21"/>
        <source>Article archived</source>
        <translation>Článek označen jako přečtený</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="24"/>
        <source>Article unarchived</source>
        <translation>Článek označen jako nepřečtený</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="183"/>
        <source>Font</source>
        <translation>Písmo</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="203"/>
        <source>Day mode</source>
        <translation>Denní režim</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="197"/>
        <source>Night mode</source>
        <translation>Noční režim</translation>
    </message>
    <message>
        <source>Archive this article</source>
        <translation type="obsolete">Označit jako přečtený</translation>
    </message>
    <message>
        <source>Remove from Favorites</source>
        <translation type="obsolete">Odstranit z oblíbených</translation>
    </message>
    <message>
        <source>Favorite this article</source>
        <translation type="obsolete">Přidat k oblíbeným</translation>
    </message>
    <message>
        <source>Re-sync articles</source>
        <translation type="obsolete">Znovu synchronizovat</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="145"/>
        <source>Loading content...</source>
        <translation>Obsah se načítá...</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="150"/>
        <source>Downloading content</source>
        <translation>Stahování obsahu</translation>
    </message>
    <message>
        <source>Can not change font size in free version</source>
        <translation type="obsolete">Nelze změnit velikost písma v bezplatné verzi</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="271"/>
        <source>Full screen</source>
        <translation>Celá obrazovka</translation>
    </message>
    <message>
        <source>Can not switch between modes in free version</source>
        <translation type="obsolete">Nelze přepínat mezi režimy v bezplatné verzi</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="110"/>
        <source> shared with #eRBook</source>
        <translation> sdílené přes #eRBook</translation>
    </message>
    <message>
        <source>Obtaining short url...</source>
        <translation type="obsolete">Získávání zkrácené url...</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="109"/>
        <source>Sharing...</source>
        <translation>Sdílení...</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="obsolete">Styl</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="223"/>
        <source>Can not change style in free version</source>
        <translation>Nelze změnit styl ve volné verzi</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="334"/>
        <location filename="../assets/ReadPage.qml" line="342"/>
        <source>Remove article</source>
        <translation>Odstranit článek</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="343"/>
        <source>Are you sure you want to remove this article and its content?</source>
        <translation>Jste si jisti, že chcete odstranit tento článek a jeho obsah?</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="345"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="422"/>
        <source>Open link</source>
        <translation>Otevřít odkaz</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="423"/>
        <source>Open link in a browser?</source>
        <translation>Otevřít odkaz v prohlížeči?</translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="425"/>
        <source>Open</source>
        <translation>Otevřít</translation>
    </message>
</context>
<context>
    <name>ReadingList</name>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="37"/>
        <source>Downloading content... %1/%2</source>
        <translation>Stahování obsahu... %1/%2</translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="57"/>
        <source>Reading List</source>
        <translation>Nepřečtené</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="obsolete">Zpět</translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="69"/>
        <source>Add article</source>
        <translation>Přidat článek</translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="77"/>
        <source>GridView</source>
        <translation>Mřížka</translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="77"/>
        <source>ListView</source>
        <translation>List</translation>
    </message>
    <message>
        <source>Download all contents</source>
        <translation type="obsolete">Stáhnout veškerý obsah</translation>
    </message>
    <message>
        <source>Re-sync articles</source>
        <translation type="obsolete">Znovu synchronizovat</translation>
    </message>
    <message>
        <source>Re-sync articles...</source>
        <translation type="obsolete">Synchronizuji...</translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="63"/>
        <source>No articles</source>
        <translation>Žádné články</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Neznámý</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Obnovit</translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="96"/>
        <source>Sync</source>
        <translation>Synchronizace</translation>
    </message>
    <message>
        <source>Obtaining short url...</source>
        <translation type="obsolete">Získávání zkrácené url...</translation>
    </message>
</context>
<context>
    <name>RegistrationHandler</name>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="37"/>
        <source>Please wait while the application connects to BBM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="116"/>
        <source>Application connected to BBM.  Press Continue.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="128"/>
        <source>Disconnected by RIM. RIM is preventing this application from connecting to BBM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="134"/>
        <source>Disconnected. Go to Settings -&gt; Security and Privacy -&gt; Application Permissions and connect this application to BBM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="142"/>
        <source>Invalid UUID. Report this error to the vendor.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="148"/>
        <source>Too many applications are connected to BBM. Uninstall one or more applications and try again.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="156"/>
        <source>Cannot connect to BBM. Download this application from AppWorld to keep using it.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="162"/>
        <source>Check your Internet connection and try again.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="169"/>
        <source>Connecting to BBM. Please wait.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="174"/>
        <source>Determining the status. Please wait.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="184"/>
        <source>Would you like to connect the application to BBM?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsAccount</name>
    <message>
        <source>Loading user info...</source>
        <translation type="obsolete">Načítání...</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="50"/>
        <source>Refresh</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="77"/>
        <source>Account info</source>
        <translation>Konto</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="123"/>
        <source>articles</source>
        <translation>články</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="341"/>
        <source>Cache</source>
        <translation>Mezipaměť</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="346"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="354"/>
        <source>Clear cache</source>
        <translation>Vymazat mezipaměť</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="355"/>
        <source>Are you sure you want to clear cache and all data assigned to articles?</source>
        <translation>Jste si jisti, že chcete vymazat mezipaměť a všechna data přiřazená k článkům?</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="357"/>
        <source>Clear</source>
        <translation>Vymazat</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="362"/>
        <source>Cache cleared</source>
        <translation>Mezipaměť vymazána</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="365"/>
        <source>Unable to clear cache</source>
        <translation>Nelze vymazat mezipaměť</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="378"/>
        <source>This removes all contents of articles</source>
        <translation>Tím se odstraní veškerý obsah článků</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="317"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="324"/>
        <source>Log out</source>
        <translation>Odhlásit se</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="17"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="20"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="152"/>
        <source>tags</source>
        <translation>značky</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="184"/>
        <source>publisher</source>
        <translation>vydavatel</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="205"/>
        <source>Logged as</source>
        <translation>Přihlášen jako</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="225"/>
        <source>Full name</source>
        <translation>Celé jméno</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="245"/>
        <source>Active subscription</source>
        <translation>Aktivní předplatné</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="265"/>
        <source>Kindle mail</source>
        <translation>Kindle mail</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="285"/>
        <source>Publish mail</source>
        <translation>Publikování</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="325"/>
        <source>Are you sure you want to log out?</source>
        <translation>Jste si jisti, že se chcete odhlásit?</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Uživatelské jméno</translation>
    </message>
    <message>
        <source>Sign out</source>
        <translation type="obsolete">Odhlásit se</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="16"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="19"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="327"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="obsolete">Odhlásit se</translation>
    </message>
    <message>
        <source>Are you sure you want to logout?</source>
        <translation type="obsolete">Jste si jisti, že chcete odhlásit?</translation>
    </message>
</context>
<context>
    <name>SettingsAppearance</name>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="39"/>
        <source>Title bar</source>
        <translation>Záhlaví</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="53"/>
        <source>White</source>
        <translation>Bílé</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="70"/>
        <source>Application theme</source>
        <translation>Téma aplikace</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="80"/>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="83"/>
        <source>Bright</source>
        <translation>Světlá</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="88"/>
        <source>Dark</source>
        <translation>Tmavá</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="94"/>
        <source>To apply change you need to restart application</source>
        <translation>Chcete-li použít změny je třeba restartovat aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="101"/>
        <source>Color scheme</source>
        <translation>Barevné schéma</translation>
    </message>
</context>
<context>
    <name>SettingsArticleView</name>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="83"/>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="92"/>
        <source>Font family</source>
        <translation>Rodina písma</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="116"/>
        <source>Font size</source>
        <translation>Velikost písma</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="137"/>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="146"/>
        <source>Reading mode</source>
        <translation>Režim čtení</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="149"/>
        <source>Day mode</source>
        <translation>Denní režim</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="153"/>
        <source>Night mode</source>
        <translation>Noční režim</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="157"/>
        <source>Sepia mode</source>
        <translation>Sepia režim</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="170"/>
        <source>Preview</source>
        <translation>Náhled</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="200"/>
        <source>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</source>
        <translation>Lorem Ipsum je demonstrativní výplňový text používaný v tiskařském a knihařském průmyslu. Lorem Ipsum je považováno za standard v této oblasti už od začátku 16. století, kdy dnes neznámý tiskař vzal kusy textu a na jejich základě vytvořil speciální vzorovou knihu. Jeho odkaz nevydržel pouze pět století, on přežil i nástup elektronické sazby v podstatě beze změny. Nejvíce popularizováno bylo Lorem Ipsum v šedesátých letech 20. století, kdy byly vydávány speciální vzorníky s jeho pasážemi a později pak díky počítačovým DTP programům jako Aldus PageMaker.</translation>
    </message>
</context>
<context>
    <name>SettingsGeneral</name>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="30"/>
        <source>Synchronization</source>
        <translation>Synchronizace</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="43"/>
        <source>Sync at startup</source>
        <translation>Synch. při spuštění</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="85"/>
        <source>Add article</source>
        <translation>Přidat článek</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="103"/>
        <source>Close after add article</source>
        <translation>Zavřít po přidání článku</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="109"/>
        <source>Close the app after add article from external application (share menu)</source>
        <translation>Zavřít aplikaci po přidání článku z externí aplikace (menu sdílení)</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="130"/>
        <source>Shortening service</source>
        <translation>Zkrácení url</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="148"/>
        <source>RDD.ME shortener</source>
        <translation>RDD.ME zkracovač</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="154"/>
        <source>Use Readability shortener service for sharing links</source>
        <translation>Použít Readability zkracovač pro sdílení odkazů</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="176"/>
        <source>Sorting</source>
        <translation>Třídění</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="177"/>
        <source>Articles</source>
        <translation>Články</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="189"/>
        <source>Tags</source>
        <translation>Značky</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="203"/>
        <source>Grouping tags</source>
        <translation>Seskupit značky</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="220"/>
        <source>Sort</source>
        <translation>Třídit</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="224"/>
        <source>Ascending</source>
        <translation>Vzestupně</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="228"/>
        <source>Descending</source>
        <translation>Sestupně</translation>
    </message>
    <message>
        <source>Sort ascending</source>
        <translation type="obsolete">Řadit vzestupně</translation>
    </message>
    <message>
        <source>Cache</source>
        <translation type="obsolete">Mezipaměť</translation>
    </message>
    <message>
        <source>Clear cache</source>
        <translation type="obsolete">Vymazat mezipaměť</translation>
    </message>
    <message>
        <source>Are you sure you want to clear all content of articles?</source>
        <translation type="obsolete">Jste si jisti, že chcete vymazat veškerý obsah článků?</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="obsolete">Vymazat</translation>
    </message>
    <message>
        <source>Cache cleared</source>
        <translation type="obsolete">Mezipaměť vymazána</translation>
    </message>
    <message>
        <source>Unable to clear cache</source>
        <translation type="obsolete">Nelze vymazat mezipaměť</translation>
    </message>
    <message>
        <source>This removes all contents of articles</source>
        <translation type="obsolete">Tím se odstraní veškerý obsah článků</translation>
    </message>
    <message>
        <source>Sign out</source>
        <translation type="obsolete">Odhlásit se</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="11"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="42"/>
        <source>General</source>
        <translation>Hlavní</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="42"/>
        <source>Synchronization, sorting, shortener etc...</source>
        <translation>Synchronizace, třídění, zkracovač atd...</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="43"/>
        <source>Appearance</source>
        <translation>Vzhled</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="43"/>
        <source>Application theme, highlight colors etc..</source>
        <translation>Téma aplikace, barvy atd...</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="45"/>
        <source>Account info, clear cache, log out etc...</source>
        <translation>Konto, mezipaměť, odhlášení atd...</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="114"/>
        <source>Can&apos;t change style of reading in free version</source>
        <translation>Nelze zmenit styl čtení v bezplatné verzi</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Zavřít</translation>
    </message>
    <message>
        <source>Synchronization</source>
        <translation type="obsolete">Synchronizace</translation>
    </message>
    <message>
        <source>Sync at startup</source>
        <translation type="obsolete">Synch. při spuštění</translation>
    </message>
    <message>
        <source>Sorting</source>
        <translation type="obsolete">Třídění</translation>
    </message>
    <message>
        <source>Sort ascending</source>
        <translation type="obsolete">Řadit vzestupně</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="45"/>
        <source>Account</source>
        <translation>Účet</translation>
    </message>
    <message>
        <source>Sign out</source>
        <translation type="obsolete">Odhlásit se</translation>
    </message>
    <message>
        <source>Sign out?</source>
        <translation type="obsolete">Odhlásit?</translation>
    </message>
    <message>
        <source>Are you sure you want to sign out ?</source>
        <translation type="obsolete">Jste si jisti, že se chcete odhlásit?</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="44"/>
        <source>Article view</source>
        <translation>Zobrazení článku</translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="44"/>
        <source>View of the article, font, mode etc...</source>
        <translation>Zobrazení článku, písmo, režim atd...</translation>
    </message>
</context>
<context>
    <name>StyleDialog</name>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="71"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="62"/>
        <source>Reading mode:</source>
        <translation>Režim čtení:</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="87"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="90"/>
        <source>Day</source>
        <translation>Den</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="94"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="113"/>
        <source>Night</source>
        <translation>Noc</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="101"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="135"/>
        <source>Sepia</source>
        <translation>Sépie</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="119"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="153"/>
        <source>Font family:</source>
        <translation>Písmo:</translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="220"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="250"/>
        <source>Font size:</source>
        <translation>Velikost písma:</translation>
    </message>
</context>
<context>
    <name>SyncDialog</name>
    <message>
        <location filename="../src/ui/syncdialog.cpp" line="18"/>
        <source>Synchronizing articles</source>
        <translation>Synchronizace článků</translation>
    </message>
    <message>
        <location filename="../src/ui/syncdialog.cpp" line="22"/>
        <source>Stop synchronization</source>
        <translation>Zastavit synchronizaci</translation>
    </message>
</context>
<context>
    <name>TagsBookmarkPage</name>
    <message>
        <location filename="../assets/Pages/TagsBookmarkPage.qml" line="23"/>
        <source>Tag removed from bookmark</source>
        <translation>Značka byla odstraněna</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsBookmarkPage.qml" line="33"/>
        <source>Bookmark tags</source>
        <translation>Značky článku</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsBookmarkPage.qml" line="36"/>
        <source>Loading bookmark tags...</source>
        <translation>Načítání značek článku...</translation>
    </message>
</context>
<context>
    <name>TagsBookmarks</name>
    <message>
        <location filename="../assets/Pages/TagsBookmarks.qml" line="14"/>
        <source>Tag: %1</source>
        <translation>Značka: %1</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsBookmarks.qml" line="42"/>
        <source>GridView</source>
        <translation>Mřížka</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsBookmarks.qml" line="42"/>
        <source>ListView</source>
        <translation>List</translation>
    </message>
</context>
<context>
    <name>TagsPage</name>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="25"/>
        <source>All tags</source>
        <translation>Všechny značky</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="30"/>
        <source>Loading tags...</source>
        <translation>Načítání značek</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="32"/>
        <source>No tags</source>
        <translation>Žádné značky</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="46"/>
        <source>Refresh</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="97"/>
        <source>Sort</source>
        <translation>Třídit</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="101"/>
        <source>Ascending</source>
        <translation>Vzestupně</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="105"/>
        <source>Descending</source>
        <translation>Sestupně</translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="80"/>
        <source>Grouping tags</source>
        <translation>Seskupit značky</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Odstranit</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Odstranit</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="29"/>
        <source>just now</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="30"/>
        <source>1 minute ago</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="86"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="93"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="101"/>
        <source>Rate app</source>
        <translation>Hodnotit aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="111"/>
        <source>Tell a Friend</source>
        <translation>Oslovit přítele</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="188"/>
        <source>Tags</source>
        <translation>Značky</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="196"/>
        <source>All your tags</source>
        <translation>Všechny vaše značky</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Obnovit</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="134"/>
        <source>Reading list</source>
        <translation>Nepřečtené články</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="155"/>
        <source>Favorite articles</source>
        <translation>Oblíbené články</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="176"/>
        <source>Archived articles</source>
        <translation>Archivované (přečtené) články</translation>
    </message>
    <message>
        <source>You are offline. Check your internet connection.</source>
        <translation type="obsolete">Jste offline. Zkontrolujte vaše připojení k internetu.</translation>
    </message>
    <message>
        <source>[500] Internal Server error. Try again later.</source>
        <translation type="obsolete">[500] Vnitřní chyba serveru. Zkuste to znovu později.</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="126"/>
        <source>Reading List</source>
        <translation>Nepřečtené</translation>
    </message>
    <message>
        <source>Shows reading list</source>
        <translation type="obsolete">Zobrazí nepřečtené články</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="147"/>
        <source>Favorites</source>
        <translation>Oblíbené</translation>
    </message>
    <message>
        <source>Shows favorite articles</source>
        <translation type="obsolete">Zobrazí oblíbené články</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="168"/>
        <source>Archives</source>
        <translation>Archiv</translation>
    </message>
    <message>
        <source>Shows archive articles</source>
        <translation type="obsolete">Zobrazí archivované články</translation>
    </message>
    <message>
        <source>Unable re-sync articles. Check your internet connection.</source>
        <translation type="obsolete">Nelze znovu synchronizovat články. Zkontrolujte vaše připojení k internetu.</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation type="obsolete">Skrýt</translation>
    </message>
</context>
</TS>
