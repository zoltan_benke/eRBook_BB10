<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../assets/AboutPage.qml" line="12"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="31"/>
        <source>Thank you for your support. Now you have a PRO version. Enjoy ;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="42"/>
        <source>No existing purchased</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="45"/>
        <source>Purchase restored. Thank you</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="80"/>
        <source>eRBook %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="91"/>
        <source>Version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="101"/>
        <source>Developer: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="112"/>
        <source>A fully featured Readability client that allows you to take your reading list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="119"/>
        <source>In case of any questions or ideas, please contact me:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="148"/>
        <source>Copyright (c) 2013 DevPDA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="164"/>
        <source>This is a FREE version of eRBook. Free version has a three limitations

&lt;i&gt;- Can&apos;t add/remove tags of articles&lt;/i&gt;
&lt;i&gt;- Can&apos;t change Style of reading&lt;/i&gt;
&lt;i&gt;- Can&apos;t change font&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="177"/>
        <source>Buy PRO version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutPage.qml" line="186"/>
        <source>Already purchased</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddPage</name>
    <message>
        <location filename="../assets/AddPage.qml" line="12"/>
        <source>You must enter a valid url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="27"/>
        <location filename="../assets/AddPage.qml" line="186"/>
        <source>Add article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="30"/>
        <source>Adding article...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="40"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="65"/>
        <location filename="../assets/Invocation/AddPage.qml" line="65"/>
        <source>url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="75"/>
        <source>Invalid URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="97"/>
        <source>Archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="126"/>
        <source>Favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="155"/>
        <source>Allow duplicates *</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AddPage.qml" line="179"/>
        <source>* Allow duplicates automatically rewrite existing article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Invocation/AddPage.qml" line="22"/>
        <source>Sharing article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Invocation/AddPage.qml" line="23"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationUI</name>
    <message>
        <location filename="../src/applicationui.cpp" line="131"/>
        <source>Launch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="136"/>
        <source>Invoke</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="141"/>
        <source>Card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="278"/>
        <source>Resized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="285"/>
        <source>Pooled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="286"/>
        <source>--</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="498"/>
        <source>Synchronization canceled. Some of the articles was not successfully synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="594"/>
        <source>Bad request. Check your URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="598"/>
        <source>Invalid login. Check you credentials and try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="608"/>
        <source>Internal server error. Try again later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="480"/>
        <source>Starting synchronization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="486"/>
        <source>Syncing article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="732"/>
        <source>Article created successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="743"/>
        <source>Article exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="751"/>
        <source>Bad request. Check the url and try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="756"/>
        <source>Internal server error. Try again later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="811"/>
        <source>Article removed successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="837"/>
        <source>Content downloaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="857"/>
        <source>Obtaining short url...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="938"/>
        <source>Sharing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="942"/>
        <location filename="../src/applicationui.cpp" line="944"/>
        <source>shared with #eRBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="991"/>
        <location filename="../src/applicationui.cpp" line="1002"/>
        <source>No more Tags can be added to this Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="994"/>
        <location filename="../src/applicationui.cpp" line="1005"/>
        <source>You have reached limit 500 tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="988"/>
        <source>Bookmark was tagged successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="730"/>
        <source>Article added successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="749"/>
        <source>Bad request. Probably the url is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="1019"/>
        <source>Tag deleted successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="1021"/>
        <source>Unable to remove this tag !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="1103"/>
        <source>Success!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.cpp" line="618"/>
        <location filename="../src/applicationui.cpp" line="745"/>
        <source>Article exists. Allow duplicates if you want to re-create it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationui.hpp" line="313"/>
        <source>Internal server error. Try again later !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Archives</name>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="35"/>
        <source>Downloading content... %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="54"/>
        <source>Archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="66"/>
        <source>Add article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="74"/>
        <source>GridView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="74"/>
        <source>ListView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="60"/>
        <source>No archived articles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Archives.qml" line="93"/>
        <source>Sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="36"/>
        <source>Downloading content... %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="55"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="67"/>
        <source>Add article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="75"/>
        <source>GridView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="75"/>
        <source>ListView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="61"/>
        <source>No favorites articles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/Favorites.qml" line="94"/>
        <source>Sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageButtonText</name>
    <message>
        <location filename="../assets/Components/ImageButtonText.qml" line="16"/>
        <source>Test</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LeadingVisual</name>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="7"/>
        <source>Sorting by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="11"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="15"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="29"/>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="33"/>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Components/LeadingVisual.qml" line="37"/>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListItemArticles</name>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="41"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="44"/>
        <source>Context menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="45"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="48"/>
        <source>Unarchive article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="45"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="48"/>
        <source>Archive article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="52"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="55"/>
        <source>Unfavorite article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="52"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="55"/>
        <source>Favorite article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="59"/>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="82"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="62"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="85"/>
        <source>Tag it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="66"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="69"/>
        <source>Can&apos;t tag articles in free version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="72"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="75"/>
        <source>Tag bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="90"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="93"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="118"/>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="126"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="121"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="129"/>
        <source>Remove article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="127"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="130"/>
        <source>Are you sure you want to remove this article and its content?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="129"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="132"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="108"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="111"/>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="101"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="104"/>
        <source>Sharing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="98"/>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="102"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="101"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="105"/>
        <source> shared with #eRBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="75"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="78"/>
        <source>comma separated tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticles.qml" line="84"/>
        <location filename="../assets/Components/ListItemArticles.qml" line="87"/>
        <source>Text will be converted to lowercase automatically</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListItemArticlesGrid</name>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="42"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="45"/>
        <source>Context menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="46"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="49"/>
        <source>Unarchive article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="46"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="49"/>
        <source>Archive article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="53"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="56"/>
        <source>Unfavorite article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="53"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="56"/>
        <source>Favorite article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="60"/>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="83"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="63"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="86"/>
        <source>Tag it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="67"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="70"/>
        <source>Can&apos;t tag articles in free version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="73"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="76"/>
        <source>Tag bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="91"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="94"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="119"/>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="127"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="122"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="130"/>
        <source>Remove article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="128"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="131"/>
        <source>Are you sure you want to remove this article and its content?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="130"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="133"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="109"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="112"/>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="102"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="105"/>
        <source>Sharing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="99"/>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="103"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="102"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="106"/>
        <source> shared with #eRBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="76"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="79"/>
        <source>comma separated tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemArticlesGrid.qml" line="85"/>
        <location filename="../assets/Components/ListItemArticlesGrid.qml" line="88"/>
        <source>Text will be converted to lowercase automatically</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListItemBookmarkTags</name>
    <message>
        <location filename="../assets/Components/ListItemBookmarkTags.qml" line="62"/>
        <source>Can&apos;t remove tags in free version of app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Components/ListItemBookmarkTags.qml" line="69"/>
        <source>Remove &apos;%1&apos; tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Components/ListItemBookmarkTags.qml" line="70"/>
        <source>Are you sure you want to remove this tag?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Components/ListItemBookmarkTags.qml" line="72"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListItemTags</name>
    <message>
        <location filename="../assets/720x720/Components/ListItemTags.qml" line="12"/>
        <location filename="../assets/Components/ListItemTags.qml" line="12"/>
        <source>Remove tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemTags.qml" line="20"/>
        <location filename="../assets/Components/ListItemTags.qml" line="24"/>
        <source>Remove &apos;%1&apos; tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemTags.qml" line="21"/>
        <location filename="../assets/Components/ListItemTags.qml" line="25"/>
        <source>Are you sure you want to remove this tag?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Components/ListItemTags.qml" line="23"/>
        <location filename="../assets/Components/ListItemTags.qml" line="27"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Components/ListItemTags.qml" line="18"/>
        <source>Can&apos;t remove tags in free version of app</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadingIndicator</name>
    <message>
        <location filename="../assets/Common/LoadingIndicator.qml" line="24"/>
        <source>Loading articles...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Common/LoadingIndicator.qml" line="27"/>
        <source>Downloading content... 0/0</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../assets/LoginPage.qml" line="16"/>
        <source>Please enter your credentials to continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="49"/>
        <source>Welcome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="52"/>
        <source>Signing in...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="71"/>
        <source>Please login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="82"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="94"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="112"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="118"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="137"/>
        <source>If you having trouble with classic login try to login via web page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="150"/>
        <source>Web login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="161"/>
        <source>Privacy policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPage.qml" line="172"/>
        <source>eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPageWeb</name>
    <message>
        <location filename="../assets/LoginPageWeb.qml" line="41"/>
        <source>Web login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPageWeb.qml" line="42"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginPageWeb.qml" line="42"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReadPage</name>
    <message>
        <location filename="../assets/ReadPage.qml" line="15"/>
        <source>Article favorited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="18"/>
        <source>Article unfavorited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="21"/>
        <source>Article archived</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="24"/>
        <source>Article unarchived</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="109"/>
        <source>Sharing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="145"/>
        <source>Loading content...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="162"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="228"/>
        <location filename="../assets/ReadPage.qml" line="250"/>
        <source>Tag it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="234"/>
        <source>Can&apos;t tag articles in free version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="240"/>
        <source>Tag bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="243"/>
        <source>comma separated tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="252"/>
        <source>Text will be converted to lowercase automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="257"/>
        <source>Show tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="265"/>
        <source>No tags for this bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="311"/>
        <source>Unarchive article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="311"/>
        <source>Archive article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="319"/>
        <source>Unfavorite article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="319"/>
        <source>Favorite article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="300"/>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="189"/>
        <source>Can not change font in free version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="197"/>
        <source>Night mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="200"/>
        <source>Sepia mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="203"/>
        <source>Day mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="327"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="150"/>
        <source>Downloading content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="271"/>
        <source>Full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="110"/>
        <source> shared with #eRBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="183"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="223"/>
        <source>Can not change style in free version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="334"/>
        <location filename="../assets/ReadPage.qml" line="342"/>
        <source>Remove article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="343"/>
        <source>Are you sure you want to remove this article and its content?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="345"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="422"/>
        <source>Open link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="423"/>
        <source>Open link in a browser?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ReadPage.qml" line="425"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReadingList</name>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="37"/>
        <source>Downloading content... %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="57"/>
        <source>Reading List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="63"/>
        <source>No articles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="69"/>
        <source>Add article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="77"/>
        <source>GridView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="77"/>
        <source>ListView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/ReadingList.qml" line="96"/>
        <source>Sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationHandler</name>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="37"/>
        <source>Please wait while the application connects to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="116"/>
        <source>Application connected to BBM.  Press Continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="128"/>
        <source>Disconnected by RIM. RIM is preventing this application from connecting to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="134"/>
        <source>Disconnected. Go to Settings -&gt; Security and Privacy -&gt; Application Permissions and connect this application to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="142"/>
        <source>Invalid UUID. Report this error to the vendor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="148"/>
        <source>Too many applications are connected to BBM. Uninstall one or more applications and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="156"/>
        <source>Cannot connect to BBM. Download this application from AppWorld to keep using it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="162"/>
        <source>Check your Internet connection and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="169"/>
        <source>Connecting to BBM. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="174"/>
        <source>Determining the status. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="184"/>
        <source>Would you like to connect the application to BBM?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsAccount</name>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="50"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="77"/>
        <source>Account info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="123"/>
        <source>articles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="341"/>
        <source>Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="346"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="354"/>
        <source>Clear cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="355"/>
        <source>Are you sure you want to clear cache and all data assigned to articles?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="357"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="362"/>
        <source>Cache cleared</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="365"/>
        <source>Unable to clear cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="378"/>
        <source>This removes all contents of articles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="317"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="324"/>
        <source>Log out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="17"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="20"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="152"/>
        <source>tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="184"/>
        <source>publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="205"/>
        <source>Logged as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="225"/>
        <source>Full name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="245"/>
        <source>Active subscription</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="265"/>
        <source>Kindle mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="285"/>
        <source>Publish mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="325"/>
        <source>Are you sure you want to log out?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAccount.qml" line="16"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="19"/>
        <location filename="../assets/Pages/SettingsAccount.qml" line="327"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsAppearance</name>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="39"/>
        <source>Title bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="53"/>
        <source>White</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="70"/>
        <source>Application theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="80"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="83"/>
        <source>Bright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="88"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="94"/>
        <source>To apply change you need to restart application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsAppearance.qml" line="101"/>
        <source>Color scheme</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsArticleView</name>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="83"/>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="92"/>
        <source>Font family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="116"/>
        <source>Font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="137"/>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="146"/>
        <source>Reading mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="149"/>
        <source>Day mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="153"/>
        <source>Night mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="157"/>
        <source>Sepia mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="170"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsArticleView.qml" line="200"/>
        <source>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsGeneral</name>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="30"/>
        <source>Synchronization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="43"/>
        <source>Sync at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="85"/>
        <source>Add article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="103"/>
        <source>Close after add article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="109"/>
        <source>Close the app after add article from external application (share menu)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="130"/>
        <source>Shortening service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="148"/>
        <source>RDD.ME shortener</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="154"/>
        <source>Use Readability shortener service for sharing links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="176"/>
        <source>Sorting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="177"/>
        <source>Articles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="189"/>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="203"/>
        <source>Grouping tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="220"/>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="224"/>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsGeneral.qml" line="228"/>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="11"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="42"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="42"/>
        <source>Synchronization, sorting, shortener etc...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="43"/>
        <source>Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="43"/>
        <source>Application theme, highlight colors etc..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="45"/>
        <source>Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="45"/>
        <source>Account info, clear cache, log out etc...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="114"/>
        <source>Can&apos;t change style of reading in free version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="44"/>
        <source>Article view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/SettingsPage.qml" line="44"/>
        <source>View of the article, font, mode etc...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StyleDialog</name>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="71"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="62"/>
        <source>Reading mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="87"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="90"/>
        <source>Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="94"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="113"/>
        <source>Night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="101"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="135"/>
        <source>Sepia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="119"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="153"/>
        <source>Font family:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/Dialogs/StyleDialog.qml" line="220"/>
        <location filename="../assets/Dialogs/StyleDialog.qml" line="250"/>
        <source>Font size:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SyncDialog</name>
    <message>
        <location filename="../src/ui/syncdialog.cpp" line="18"/>
        <source>Synchronizing articles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/syncdialog.cpp" line="22"/>
        <source>Stop synchronization</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagsBookmarkPage</name>
    <message>
        <location filename="../assets/Pages/TagsBookmarkPage.qml" line="23"/>
        <source>Tag removed from bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsBookmarkPage.qml" line="33"/>
        <source>Bookmark tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsBookmarkPage.qml" line="36"/>
        <source>Loading bookmark tags...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagsBookmarks</name>
    <message>
        <location filename="../assets/Pages/TagsBookmarks.qml" line="14"/>
        <source>Tag: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsBookmarks.qml" line="42"/>
        <source>GridView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsBookmarks.qml" line="42"/>
        <source>ListView</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagsPage</name>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="25"/>
        <source>All tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="30"/>
        <source>Loading tags...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="32"/>
        <source>No tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="46"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="97"/>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="101"/>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="105"/>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Pages/TagsPage.qml" line="80"/>
        <source>Grouping tags</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="29"/>
        <source>just now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="30"/>
        <source>1 minute ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="86"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="93"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="101"/>
        <source>Rate app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="111"/>
        <source>Tell a Friend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="126"/>
        <source>Reading List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="134"/>
        <source>Reading list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="147"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="155"/>
        <source>Favorite articles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="168"/>
        <source>Archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="176"/>
        <source>Archived articles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="188"/>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="196"/>
        <source>All your tags</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
