import bb.cascades 1.2
import bb.system 1.2

import "Common"
import "Components"

Sheet {
    id: sheet
    peekEnabled: false
    
    
    function login(){
        if (username.text.length && password.text.length){
            App.login(username.text, password.text);
        }else{
            Helper.showToast(qsTr("Please enter your credentials to continue"));
        }
    }
    
    
    onOpened: {
        Database.authorizedChanged.connect(close)
    }
    
    onClosed: {
        App.bookmarks(true)
        destroy()
    }
    
    MyPage {
        id: page
        
        
        attachedObjects: [
            Invocation {
                id: invoke
                query{
                    mimeType: "text/html"
                    uri: "https://www.readability.com/register"
                }
            },
            ComponentDefinition {
                id: webLoginSheet
                source: "LoginPageWeb.qml"
            }
        ]
        
        header: TitleHeader {
            titleText: qsTr("Welcome") + Retranslate.onLocaleOrLanguageChanged
        }
        loading: App.loading
        loadingText: qsTr("Signing in...") + Retranslate.onLocaleOrLanguageChanged
        leftPadding: 10
        rightPadding: 10
        
        ScrollView {
            enabled: !App.loading
            opacity: enabled ? 1 : 0.3
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            scrollViewProperties.pinchToZoomEnabled: false
            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                
                
                Label{
                    text: qsTr("Please login") + Retranslate.onLocaleOrLanguageChanged
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle{
                        base: SystemDefaults.TextStyles.BigText
                    }
                }
                Divider {}
                
                TextField {
                    id: username
                    maximumLength: 40
                    hintText: qsTr("Username") + Retranslate.onLocaleOrLanguageChanged
                    input{
                        submitKey: SubmitKey.Next
                        onSubmitted: {
                            password.requestFocus()
                        }
                    }
                }
                TextField {
                    id: password
                    
                    maximumLength: 40
                    hintText: qsTr("Password") + Retranslate.onLocaleOrLanguageChanged
                    inputMode: TextFieldInputMode.Password
                    input{
                        submitKey: SubmitKey.Submit
                        onSubmitted: {
                            sheet.login()
                        }
                    }
                }
                
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    
                    Button {
                        text: qsTr("Login") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            sheet.login()
                        }
                    }
                    Button {
                        text: qsTr("Register") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            invoke.trigger("bb.action.OPEN")
                        }
                    }
                }
                
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    topPadding: 10
                    leftPadding: 10
                    rightPadding: 10
                    
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        multiline: true
                        text: qsTr("If you having trouble with classic login try to login via web page") + Retranslate.onLocaleOrLanguageChanged
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        textStyle{
                            base: SystemDefaults.TextStyles.SubtitleText
                        }
                    }
                    
                    
                    Button {
                        id: troubleBtn
                        verticalAlignment: VerticalAlignment.Center
                        text: qsTr("Web login") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            var webSheet = webLoginSheet.createObject()
                            webSheet.open()
                        }
                    }
                
                }
                
                
                Label{
                    text: qsTr("Privacy policy") + Retranslate.onLocaleOrLanguageChanged
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle{
                        base: SystemDefaults.TextStyles.BigText
                    }
                }
                
                Divider {}
                
                Label{
                    multiline: true
                    text: qsTr("eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook") + Retranslate.onLocaleOrLanguageChanged
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle.textAlign: TextAlign.Center
                }
            }
        }
    }
}