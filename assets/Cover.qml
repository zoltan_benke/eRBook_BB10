import bb.cascades 1.2


Container {
    
    ImageView {
        loadEffect: ImageViewLoadEffect.FadeZoom
        imageSource: {
            if (SizeHelper.maxHeight == 1280 && SizeHelper.maxWidth == 720){
                console.debug("Z30 cover")
                return "asset:///Images/covers/Z30_cover.png"
            }
            else if(SizeHelper.maxHeight == 1280 && SizeHelper.maxWidth == 768){
                console.debug("Z10 cover")
                return "asset:///Images/covers/Z10_cover.png"
            }
            else{
                console.debug("Q10/Q5 cover")
                return "asset:///Images/covers/Q10_cover.png"
            }
        }
    }
}
