import bb.cascades 1.2
import com.devpda.tools 1.2
import bb.system 1.2

/*
 {
 "user_id": 843398,
 "read_percent": "0.00",
 "tags": [],
 "date_updated": "2014-01-15 08:53:51",
 "favorite": true,
 "id": 51863474,
 "date_archived": "2014-01-15 08:53:51",
 "date_opened": null,
 "article": {
 "domain": "www.fdb.cz",
 "title": "Filmová databáze FDb.cz",
 "url": "http://www.fdb.cz/",
 "lead_image_url": "http://img.fdb.cz/ad/matvi.png",
 "author": null,
 "excerpt": "V&#xED;ce &gt;",
 "direction": "ltr",
 "word_count": 162,
 "date_published": null,
 "dek": null,
 "processed": true,
 "id": "c2una3ct"
 },
 "article_href": "/api/rest/v1/articles/c2una3ct",
 "date_favorited": "2013-11-01 09:49:34",
 "date_added": "2014-01-15 08:53:51",
 "archive": true
 }
 */



ListItemBase {
    id: root
    
    property bool showTags: true
    
    contextActions: [
        ActionSet {
            title: qsTr("Context menu") + Retranslate.onLocaleOrLanguageChanged
            subtitle: ListItemData.article.title
            actions: [
                ActionItem {
                    title: ListItemData.archive ? qsTr("Unarchive article") + Retranslate.onLocaleOrLanguageChanged : qsTr("Archive article") + Retranslate.onLocaleOrLanguageChanged
                    imageSource: ListItemData.archive ? "asset:///Images/readingList.png" : "asset:///Images/no-archive.png"
                    onTriggered: {
                        Qt.App.updateBookmark(ListItemData.id, ListItemData.favorite ? 1 : 0, ListItemData.archive ? 0 : 1)
                    }
                },
                ActionItem {
                    title: ListItemData.favorite ? qsTr("Unfavorite article") + Retranslate.onLocaleOrLanguageChanged : qsTr("Favorite article") + Retranslate.onLocaleOrLanguageChanged
                    imageSource: "asset:///Images/favorite.png"
                    onTriggered: {
                        Qt.App.updateBookmark(ListItemData.id, ListItemData.favorite ? 0 : 1, ListItemData.archive ? 1 : 0)
                    }
                },
                ActionItem {
                    title: qsTr("Tag it") + Retranslate.onLocaleOrLanguageChanged
                    imageSource: "asset:///Images/tag_white.png"
                    onTriggered: {
                        if (Qt.Settings.isPro){
                            Qt.App.indexPath = root.ListItem.indexPath
                            tagDialog.show()
                        }else{
                            Qt.Helper.showToast(qsTr("Can't tag articles in free version") + Retranslate.onLocaleOrLanguageChanged)
                        }
                    }
                    attachedObjects: [
                        SystemPrompt {
                            id: tagDialog
                            title: qsTr("Tag bookmark") + Retranslate.onLocaleOrLanguageChanged
                            rememberMeChecked: false
                            includeRememberMe: false
                            inputField.emptyText: qsTr("comma separated tags") + Retranslate.onLocaleOrLanguageChanged
                            onFinished: {
                                if (value == SystemUiResult.ConfirmButtonSelection){
                                    console.debug("Input text".concat(inputFieldTextEntry()))
                                    Qt.App.addTagsToBookmark(ListItemData.id, inputFieldTextEntry().toLowerCase().trim());
                                }
                            }
                            confirmButton.label: qsTr("Tag it")
                            returnKeyAction: SystemUiReturnKeyAction.Done
                            body: qsTr("Text will be converted to lowercase automatically") + Retranslate.onLocaleOrLanguageChanged
                        }
                    ]
                },
                ActionItem {
                    id: shareAction
                    title: qsTr("Share") + Retranslate.onLocaleOrLanguageChanged
                    imageSource: "asset:///Images/share.png"
                    onTriggered: {
                        Qt.App.indexPath = root.ListItem.indexPath
                        if (Qt.Settings.useShortener){
                            if (!Qt.Database.shortUrlExists(ListItemData.article.id)){
                                Qt.App.getShortUrl(ListItemData.article.url)
                            }else{
                                Qt.App.share(ListItemData.article.title.concat("\n").concat(Qt.Database.shortUrl(ListItemData.article.id)).concat(qsTr(" shared with #eRBook") + Retranslate.onLocaleOrLanguageChanged))
                            }
                        }else{
                            Qt.Helper.showToast(qsTr("Sharing...") + Retranslate.onLocaleOrLanguageChanged)
                            Qt.App.share(ListItemData.article.title.concat("\n").concat(ListItemData.article.url).concat(qsTr(" shared with #eRBook") + Retranslate.onLocaleOrLanguageChanged))
                        }
                    }
                },
                ActionItem {
                    ActionBar.placement: ActionBarPlacement.OnBar
                    title: qsTr("Open in browser") + Retranslate.onLocaleOrLanguageChanged
                    imageSource: "asset:///Images/open_in_browser.png"
                    onTriggered: {
                        invoker.action = "bb.action.OPEN"
                        invoker.target = "sys.browser"
                        invoker.uri = ListItemData.article.url
                        invoker.invoke()
                    }
                },
                DeleteActionItem {
                    title: qsTr("Remove article") + Retranslate.onLocaleOrLanguageChanged
                    imageSource: "asset:///Images/remove.png"
                    onTriggered: {
                        dialog.show()
                    }
                    attachedObjects: [
                        SystemDialog {
                            id: dialog
                            title: qsTr("Remove article") + Retranslate.onLocaleOrLanguageChanged
                            body: qsTr("Are you sure you want to remove this article and its content?") + Retranslate.onLocaleOrLanguageChanged
                            confirmButton{
                                label: qsTr("Remove") + Retranslate.onLocaleOrLanguageChanged
                            }
                            onFinished: {
                                if (value == SystemUiResult.ConfirmButtonSelection){
                                    Qt.App.indexPath = root.ListItem.indexPath
                                    Qt.App.removeBookmark(ListItemData.id)
                                }else{
                                    console.log("CANCEL CLICKED")
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
    
    RemoteImage {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        opacity: 0.3
        scalingMethod: ScalingMethod.AspectFill
        url: ListItemData.article.lead_image_url ? ListItemData.article.lead_image_url : ""
    }
    
    Container {
        layout: DockLayout {
        
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        Container {
            verticalAlignment: VerticalAlignment.Top
            horizontalAlignment: HorizontalAlignment.Fill
            Label {
                text: ListItemData.article.title
                textStyle.base: SystemDefaults.TextStyles.TitleText
                multiline: true
                autoSize.maxLineCount: 2
                textStyle.fontWeight: FontWeight.W600
                bottomMargin: 1
                textFit.mode: LabelTextFitMode.Standard
            }
            
            Label {
                topMargin: 1
                text: ListItemData.article.excerpt
                multiline: true
                autoSize.maxLineCount: 4
                textStyle.fontWeight: FontWeight.W400
                textFormat: TextFormat.Html
            }
        }
        
        
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            visible: ListItemData.tagy && showTags
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Fill
            bottomPadding: 35
            ImageView {
                preferredHeight: 40
                preferredWidth: 40
                imageSource: Qt.App.whiteTheme ? "asset:///Images/tag_black.png" : "asset:///Images/tag_white.png"
                verticalAlignment: VerticalAlignment.Center
                bottomMargin: 0
            }
            
            Label {
                leftMargin: 5
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                multiline: true
                autoSize.maxLineCount: 2
                verticalAlignment: VerticalAlignment.Center
                textStyle{
                    color: Color.Gray
                    base: SystemDefaults.TextStyles.SubtitleText
                }
                text: ListItemData.tagy
                bottomMargin: 0
            }
        }
        
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            background: back.imagePaint
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Fill
            leftPadding: 5
            rightPadding: 5
            attachedObjects: [
                ImagePaintDefinition {
                    id: back
                    repeatPattern: RepeatPattern.X
                    imageSource: Qt.App.whiteTheme ? "asset:///Images/patterns/262626.png" : "asset:///Images/patterns/f8f8f8.png"
                }
            ]
            ImageView {
                visible: ListItemData.favorite
                verticalAlignment: VerticalAlignment.Center
                imageSource: "asset:///Images/Stars/".concat(Qt.Settings.activeColor).concat(".png")
                layoutProperties: StackLayoutProperties {
                    spaceQuota: -1
                }
            }
            
            Label {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                horizontalAlignment: HorizontalAlignment.Center
                text: ListItemData.article.domain
                verticalAlignment: VerticalAlignment.Center
                textStyle{
                    color: Qt.App.whiteTheme ? Color.White : Color.Black
                    base: SystemDefaults.TextStyles.SubtitleText
                }
            }
            
            ImageView {
                visible: !Qt.Database.isCached(ListItemData.article.id)
                verticalAlignment: VerticalAlignment.Center
                imageSource: "asset:///Images/no_content.png"
                layoutProperties: StackLayoutProperties {
                    spaceQuota: -1
                }
            }
        } // End of Star and Domain container
    }
}
