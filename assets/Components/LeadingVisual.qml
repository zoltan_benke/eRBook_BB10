import bb.cascades 1.2

Container {
    id: root
    visible: (App.modelSize)
    DropDown {
        title: qsTr("Sorting by") + Retranslate.onLocaleOrLanguageChanged
        selectedIndex: (Settings.sortingKeys == "date_added") ? 0 : 1
        options: [
            Option {
                text: qsTr("Date") + Retranslate.onLocaleOrLanguageChanged
                value: "date_added"
            },
            Option {
                text: qsTr("Title") + Retranslate.onLocaleOrLanguageChanged
                value: "title"
            }
        ]
        onSelectedIndexChanged: {
            if (selectedValue != Settings.sortingKeys){
                console.log("SELECTED VALUE: ".concat(selectedValue))
                Settings.sortingKeys = [selectedValue.toString()]
                App.setSortingKeys(Settings.sortingKeys)
            }
        }
    }
    
    DropDown {
        title: qsTr("Sort") + Retranslate.onLocaleOrLanguageChanged
        selectedIndex: Settings.sortAscending ? 0 : 1
        options: [
            Option {
                text: qsTr("Ascending") + Retranslate.onLocaleOrLanguageChanged
                value: true
            },
            Option {
                text: qsTr("Descending") + Retranslate.onLocaleOrLanguageChanged
                value: false
            }
        ]
        onSelectedIndexChanged: {
            if (selectedValue != Settings.sortAscending){
                console.log("SELECTED VALUE: ".concat(selectedValue))
                Settings.sortAscending = selectedValue
                App.setSortAscending(Settings.sortAscending)
            }
        }
    }
}
