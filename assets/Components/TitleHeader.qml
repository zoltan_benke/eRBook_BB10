import bb.cascades 1.2
import com.devpda.tools 1.2

TitleBar {
    id: root
    
    property alias titleText: titleLabel.text
    property alias subtitleText: subtitleLabel.text
    
    property alias imageVisible: image.visible
    property alias imageSource: image.url
    
    property alias counterVisible: counter.visible
    property alias counterText: counter.text
    
    property alias headerLoading: loading.visible
    
    property alias leftButtonText: leftButton.text
    property alias leftButtonImageSource: leftButton.imageSource
    property alias rightButtonImageSource: rightButton.defaultImageSource
    
    signal leftButtonClicked()
    signal rightButtonClicked()
    
    kind: TitleBarKind.FreeForm
    kindProperties: FreeFormTitleBarKindProperties {
        id: freeForm

        content: Container {
            id: container
            
            attachedObjects: [
                ImagePaintDefinition {
                    id: back
                    repeatPattern: RepeatPattern.X
                    imageSource: !Settings.whiteHeader ? "asset:///Images/patterns/262626.png" : "asset:///Images/patterns/f8f8f8.png"
                }
            ]
            layout: DockLayout{}
            background: back.imagePaint
            horizontalAlignment: HorizontalAlignment.Fill
            
            Container {
                layout: DockLayout{}
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Bottom
                preferredHeight: 10
                background: Color.create("#".concat(Settings.activeColor))
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                verticalAlignment: VerticalAlignment.Center
                leftPadding: 15
                rightPadding: 15
                RemoteImage {
                    id: image
                    visible: false
                    preferredHeight: SizeHelper.nType ? 80 : 100
                    preferredWidth: SizeHelper.nType ? 80 : 100
                    horizontalAlignment: HorizontalAlignment.Left
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                }
                HeaderButton {
                    id: leftButton
                    visible: (text != "")
                    rightPadding: 5
                    preferredWidth: 200
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onClicked: {
                        root.leftButtonClicked()
                    }
                }/*
                ImageButton {
                    id: leftButton
                    visible: defaultImageSource != ""
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onClicked: {
                        root.leftButtonClicked()
                    }
                    accessibility.name: "image button"
                }*/
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    
                    Label {
                        id: titleLabel
                        textStyle.color: !Settings.whiteHeader ? Color.White : Color.Black
                        textStyle.base: SystemDefaults.TextStyles.PrimaryText
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                        bottomMargin: 0
                        accessibility.name: "title label"
                    }
                    
                    Label {
                        id: subtitleLabel
                        visible: text
                        textStyle.color: Color.Gray
                        textStyle.base: SystemDefaults.TextStyles.SubtitleText
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: -1
                        }
                        topMargin: 0
                        accessibility.name: "subtitle label"
                    }
                } // End of Title/Subtitle container
                ImageButton {
                    id: rightButton
                    visible: defaultImageSource != ""
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onClicked: {
                        root.rightButtonClicked()
                    }
                    accessibility.name: "image button"
                }
                Counter {
                    id: counter
                    visible: false
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                }
                ActivityIndicator {
                    id: loading
                    visible: false
                    running: visible
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                }
            }
        }
    }
    scrollBehavior: TitleBarScrollBehavior.Sticky
}