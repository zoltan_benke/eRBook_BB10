import bb.cascades 1.2
import bb.cascades.pickers 1.0

import "../Common"
import "../Components"

MyPage {
    
    header: TitleHeader {
        id: titleHeader
        titleText: qsTr("Settings") + Retranslate.onLocaleOrLanguageChanged
    }
    loading: false
    
    attachedObjects: [
        ComponentDefinition {
            id: general
            source: "SettingsGeneral.qml"
        },
        ComponentDefinition {
            id: appearance
            source: "SettingsAppearance.qml"
        },
        ComponentDefinition {
            id: account
            source: "SettingsAccount.qml"
        },
        ComponentDefinition {
            id: article
            source: "SettingsArticleView.qml"
        }
    ]
    onActiveChanged: {
        if (active)
            settingsAction.enabled = false
        else
            settingsAction.enabled = true
    }
    ListView {
        id: listVIew
        onCreationCompleted: {
            dataModel.append([{"name": qsTr("General"), "subtitle": qsTr("Synchronization, sorting, shortener etc..."), "icon": App.whiteTheme ? "asset:///Images/settings_black.png" : "asset:///Images/settings_white.png"}, 
                    {"name": qsTr("Appearance"), "subtitle": qsTr("Application theme, highlight colors etc.."), "icon": App.whiteTheme ? "asset:///Images/appearance_black.png" : "asset:///Images/appearance_white.png"},
                    {"name": qsTr("Article view"), "subtitle": qsTr("View of the article, font, mode etc..."), "icon": App.whiteTheme ? "asset:///Images/style_black.png" : "asset:///Images/style_white.png"},
                    {"name": qsTr("Account"), "subtitle": qsTr("Account info, clear cache, log out etc..."), "icon": App.whiteTheme ? "asset:///Images/account_black.png" : "asset:///Images/account_white.png"}
                    ])
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: ArrayDataModel {
            id: model
        }
        layout: StackListLayout {
        }
        listItemComponents: [
            ListItemComponent {
                ListItemBase {
                    id: rootItem
                    preferredHeight: Qt.SizeHelper.nType ? 110 : 140
                    backColor: SystemDefaults.Paints.ContainerBackground
                    enabled: (parseInt(rootItem.ListItem.indexPath) != 2)
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                        
                        ImageView {
                            verticalAlignment: VerticalAlignment.Center
                            imageSource: ListItemData.icon
                            preferredHeight: Qt.SizeHelper.nType ? 80 : 110
                            preferredWidth: Qt.SizeHelper.nType ? 80 : 110
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: -1
                            }
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label{
                                text: ListItemData.name
                                textStyle.base: SystemDefaults.TextStyles.PrimaryText
                                bottomMargin: 0
                            }
                            
                            Label {
                                topMargin: 0
                                text: ListItemData.subtitle
                                textStyle.base: SystemDefaults.TextStyles.SubtitleText
                            }
                        }
                    } // End of Container
                } // End of ListItemBase
            } // ListItemComponent
        ]
        onTriggered: {
            console.debug("INDEX: ".concat(indexPath))
            var data = dataModel.data(indexPath)
            var page;
            switch (parseInt(indexPath)){
                case 0: // General settings
                    page = general.createObject()
                    break;
                case 1:
                    page = appearance.createObject()
                    break;
                case 2:
                    if (Settings.isPro){
                        page = article.createObject()
                    }else{
                        Helper.showToast(qsTr("Can't change style of reading in free version"))
                    }
                    break;
                case 3:
                    page = account.createObject()
                    break;
                default:
                    console.log("Unknown index")
                    return;
            }
            page.title = titleHeader.titleText
            page.subtitle = data.name
            page.image = data.icon
            tabPane.naviPane.push(page)
        }
    }
}