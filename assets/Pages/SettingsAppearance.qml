import bb.cascades 1.2
import bb.system 1.2

import "../Components"
import "../Common"

MyPage {
    id: root
    
    function showToast(){
    }
    
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    
    
    onCreationCompleted: {
        var theme = ThemeSettings.getThemeString("theme", App.whiteTheme ? "bright" : "dark")
        console.debug("THEME: ".concat(theme))
        themeDropDown.setSelectedIndex(theme == "bright" ? 0 : 1)
    }
    
    
    ScrollView {
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            
            Header {
                title: qsTr("Title bar") + Retranslate.onLocaleOrLanguageChanged
            }
            
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                
                Label {
                    verticalAlignment: VerticalAlignment.Center
                    text: qsTr("White") + Retranslate.onLocaleOrLanguageChanged
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                ToggleButton {
                    verticalAlignment: VerticalAlignment.Center
                    checked: Settings.whiteHeader
                    onCheckedChanged: {
                        Settings.whiteHeader = checked
                    }
                }
            }
            
            
            Header {
                title: qsTr("Application theme") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                DropDown {
                    id: themeDropDown
                    
                    title: qsTr("Theme") + Retranslate.onLocaleOrLanguageChanged
                    
                    Option {
                        text: qsTr("Bright") + Retranslate.onLocaleOrLanguageChanged
                        value: VisualStyle.Bright
                    }
                    
                    Option {
                        text: qsTr("Dark") + Retranslate.onLocaleOrLanguageChanged
                        value: VisualStyle.Dark
                    }
                    onSelectedIndexChanged: {
                        var theme = App.whiteTheme ? VisualStyle.Bright : VisualStyle.Dark
                        if (theme != selectedOption.value)
                            Helper.showToast(qsTr("To apply change you need to restart application") + Retranslate.onLocaleOrLanguageChanged)
                        ThemeSettings.setThemeString("theme", selectedOption.value === VisualStyle.Bright ? "bright" : "dark")
                    }
                }
            }
            
            Header {
                title: qsTr("Color scheme") + Retranslate.onLocaleOrLanguageChanged
            }
            
            ListView {
                id: listView
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                onCreationCompleted: {
                    dataModel.append(["0098f0", "96b800", "a30d7e", "cc3f10", "ff0080", "660099", "ffcc00"])
                }
                dataModel: ArrayDataModel {
                    id: model
                }
                layout: GridListLayout {
                    columnCount: 7
                    
                    horizontalCellSpacing: 10
                    verticalCellSpacing: 10
                    orientation: LayoutOrientation.TopToBottom
                    cellAspectRatio: 1
                }
                listItemComponents: [
                    ListItemComponent {
                        type: ""
                        ImageView {
                            id: image
                            scaleX: (Qt.Settings.activeColor === ListItemData) ? 0.7 : 1
                            scaleY: scaleX
                            imageSource: "asset:///Images/Colors/".concat(ListItemData).concat(".png")
                            scalingMethod: ScalingMethod.AspectFit
                        }
                    }
                ]
                onTriggered: {
                    var item = dataModel.data(indexPath)
                    Settings.activeColor = item;
                }
                scrollRole: ScrollRole.None
                flickMode: FlickMode.None
                snapMode: SnapMode.None
                scrollIndicatorMode: ScrollIndicatorMode.None
                layoutProperties: StackLayoutProperties{
                    spaceQuota: -1
                }
            } // End of ListView
        }
    }
}