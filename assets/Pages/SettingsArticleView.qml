import bb.cascades 1.2

import "../Components"
import "../Common"

MyPage {
    id: root
    
    function setStyleSheet(readMode, readFamily){
        var styleSheet;
        var backColor;
        if(readMode === 1){
            backColor = "#262626"
            if (readFamily === 0){
                styleSheet = "../css/night.css"
            }else if (readFamily === 1){
                styleSheet = "../css/night-tnr.css"
            }else if (readFamily === 2){
                styleSheet = "../css/night-csm.css"
            }else if (readFamily === 3){
                styleSheet = "../css/night-verdana.css"
            }else if (readFamily === 4){
                styleSheet = "../css/night-myriad.css"
            }
        }else if (readMode === 2){
            backColor = "#FBF0D9"
            if (readFamily === 0){
                styleSheet = "../css/sepia.css"
            }else if (readFamily === 1){
                styleSheet = "../css/sepia-tnr.css"
            }else if (readFamily === 2){
                styleSheet = "../css/sepia-csm.css"
            }else if (readFamily === 3){
                styleSheet = "../css/sepia-verdana.css"
            }else if (readFamily === 4){
                styleSheet = "../css/sepia-myriad.css"
            }
        }else{
            backColor = "#f8f8f8"
            if (readFamily === 0){
                styleSheet = "../css/day.css"
            }else if (readFamily === 1){
                styleSheet = "../css/day-tnr.css"
            }else if (readFamily === 2){
                styleSheet = "../css/day-csm.css"
            }else if (readFamily === 3){
                styleSheet = "../css/day-verdana.css"
            }else if (readFamily === 4){
                styleSheet = "../css/day-myriad.css"
            }
        }
        Settings.backColor = backColor
        webView.settings.userStyleSheetLocation = styleSheet
    }
    
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    
    
    onCreationCompleted: {
        setStyleSheet(Settings.readMode, Settings.readFamily)
        Settings.styleSheetChanged.connect(setStyleSheet)
        fontFamily.selectedIndex = Settings.readFamily
        readingMode.selectedIndex = Settings.readMode
    }
    
    
    ScrollView {
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            
            Header {
                title: qsTr("Font family") + Retranslate.onLocaleOrLanguageChanged 
            }
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                DropDown {
                    id: fontFamily
                    title: qsTr("Font family") + Retranslate.onLocaleOrLanguageChanged 
                    options: [
                        Option {
                            text: "Arial"
                            value: 0
                        },
                        Option {
                            text: "Times New Roman"
                            value: 1
                        },
                        Option {
                            text: "Comic Sans MS"
                            value: 2
                        }
                    ]
                    onSelectedOptionChanged: {
                        if (selectedValue != Settings.readFamily){
                            Settings.readFamily = selectedValue
                        }
                    }
                }
            } // End of font family dropdown
            
            Header {
                title: qsTr("Font size") + Retranslate.onLocaleOrLanguageChanged 
            }
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 25
                
                Slider {
                    horizontalAlignment: HorizontalAlignment.Center
                    fromValue: 32
                    toValue: 64
                    value: Settings.fontSize
                    onImmediateValueChanged: {
                        Settings.fontSize = (immediateValue)
                        console.log("FONT SIZE: ".concat(immediateValue))
                    }
                }
            } // End of slider container
            
            Header {
                title: qsTr("Reading mode") + Retranslate.onLocaleOrLanguageChanged 
            }

            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                DropDown {
                    id: readingMode
                    title: qsTr("Reading mode") + Retranslate.onLocaleOrLanguageChanged 
                    options: [
                        Option {
                        text: qsTr("Day mode") + Retranslate.onLocaleOrLanguageChanged
                            value: 0
                        },
                        Option {
                        text: qsTr("Night mode") + Retranslate.onLocaleOrLanguageChanged
                            value: 1
                        },
                        Option {
                        text: qsTr("Sepia mode") + Retranslate.onLocaleOrLanguageChanged
                            value: 2
                        }
                    ]
                    onSelectedOptionChanged: {
                        if (selectedValue != Settings.readMode){
                            Settings.readMode = selectedValue
                        }
                    }
                }
            } // End of font family dropdown
            
            Header {
                title: qsTr("Preview") + Retranslate.onLocaleOrLanguageChanged 
            }
            
            
            Container {
                horizontalAlignment: HorizontalAlignment.Center
                preferredWidth: SizeHelper.maxWidth - 40
                maxHeight: 400
                background: Color.create(Settings.backColor)
                leftPadding: 20
                rightPadding: 20
                topPadding: 20
                WebView {
                    id: webView
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                    settings.javaScriptEnabled: true
                    settings.cookiesEnabled: false
                    settings.binaryFontDownloadingEnabled: true
                    settings.textAutosizingEnabled: true
                    settings.zoomToFitEnabled: true
                    settings.activeTextEnabled: true
                    settings.devicePixelRatio: 1.0
                    settings.viewport: {
                        "width": "device-width",
                        "initial-scale": 1.0
                    }
                    settings.defaultTextCodecName: "UTF-8"
                    settings.background: Color.Transparent
                    settings.defaultFontSize: Settings.fontSize
                    html: qsTr("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.") + Retranslate.onLocaleOrLanguageChanged
                }
            }
        }
    }
}