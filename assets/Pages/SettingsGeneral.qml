import bb.cascades 1.2
import bb.system 1.2
import "../Components"
import "../Common"


MyPage {
    id: root
    

    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    
    
    ScrollView {
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            
            Header {
                title: qsTr("Synchronization") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                
                Label {
                    verticalAlignment: VerticalAlignment.Center
                    text: qsTr("Sync at startup") + Retranslate.onLocaleOrLanguageChanged
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                ToggleButton {
                    checked: Settings.syncStartup
                    onCheckedChanged: {
                        Settings.syncStartup = checked
                    }
                }
            } // End of Synchronization settings
            
            /*
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                
                Label {
                    verticalAlignment: VerticalAlignment.Center
                    text: qsTr("Sync after add article") + Retranslate.onLocaleOrLanguageChanged
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                ToggleButton {
                    checked: Settings.syncAfterAdd
                    onCheckedChanged: {
                        Settings.syncAfterAdd = checked
                    }
                }
            } // End of Sync after add settings
            
            */
            
            Header {
                title: qsTr("Add article") + Retranslate.onLocaleOrLanguageChanged
            }
            
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Close after add article") + Retranslate.onLocaleOrLanguageChanged
                        bottomMargin: 0
                    }
                    Label {
                        topMargin: 0
                        multiline: true
                        text: qsTr("Close the app after add article from external application (share menu)") + Retranslate.onLocaleOrLanguageChanged
                        textStyle{
                            base: SystemDefaults.TextStyles.SubtitleText
                            color: Color.Gray
                        }
                    }
                }
                ToggleButton {
                    horizontalAlignment: horizontalAlignment.Right
                    checked: Settings.closeInvoke
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onCheckedChanged: {
                        Settings.closeInvoke = checked
                    }
                }
            }
            
            
            Header {
                title: qsTr("Shortening service") + Retranslate.onLocaleOrLanguageChanged
            }
            
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("RDD.ME shortener") + Retranslate.onLocaleOrLanguageChanged
                        bottomMargin: 0
                    }
                    Label {
                        topMargin: 0
                        multiline: true
                        text: qsTr("Use Readability shortener service for sharing links") + Retranslate.onLocaleOrLanguageChanged
                        textStyle{
                            base: SystemDefaults.TextStyles.SubtitleText
                            color: Color.Gray
                        }
                    }
                }
                ToggleButton {
                    horizontalAlignment: horizontalAlignment.Right
                    checked: Settings.useShortener
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onCheckedChanged: {
                        Settings.useShortener = checked
                    }
                }
            }
            
            
            Header {
                id: sortingHeader
                title: qsTr("Sorting") + Retranslate.onLocaleOrLanguageChanged
                subtitle: qsTr("Articles") + Retranslate.onLocaleOrLanguageChanged
            }
            
            LeadingVisual {
                visible: true
                topPadding: 10
                leftPadding: 10
                rightPadding: 10
            }
            
            Header {
                title: sortingHeader.title
                subtitle: qsTr("Tags") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    
                    Label {
                        text: qsTr("Grouping tags") + Retranslate.onLocaleOrLanguageChanged
                        verticalAlignment: VerticalAlignment.Center
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                    }
                    
                    ToggleButton {
                        verticalAlignment: VerticalAlignment.Center
                        checked: Settings.tagsGrouping
                        onCheckedChanged: {
                            Settings.tagsGrouping = checked
                            App.setTagsGrouping(Settings.tagsGrouping ? "first" : "none")
                        }
                    }
                }
                DropDown {
                    title: qsTr("Sort") + Retranslate.onLocaleOrLanguageChanged
                    selectedIndex: Settings.tagsSortAscending ? 0 : 1
                    options: [
                        Option {
                            text: qsTr("Ascending") + Retranslate.onLocaleOrLanguageChanged
                            value: true
                        },
                        Option {
                            text: qsTr("Descending") + Retranslate.onLocaleOrLanguageChanged
                            value: false
                        }
                    ]
                    onSelectedIndexChanged: {
                        if (selectedValue != Settings.tagsSortAscending){
                            console.log("SELECTED VALUE: ".concat(selectedValue))
                            Settings.tagsSortAscending = selectedValue
                            App.setTagsSortAscending(Settings.tagsSortAscending)
                        }
                    }
                }
            }
            
            /*
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                
                Label {
                    verticalAlignment: VerticalAlignment.Center
                    text: qsTr("Sort ascending") + Retranslate.onLocaleOrLanguageChanged
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                ToggleButton {
                    checked: Settings.sortAscending
                    onCheckedChanged: {
                        Settings.sortAscending = checked
                        App.setSortAscending(checked)
                    }
                }
            }*/
            
            /*Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                
                Label {
                    verticalAlignment: VerticalAlignment.Center
                    text: qsTr("Grouping items") + Retranslate.onLocaleOrLanguageChanged
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                ToggleButton {
                    checked: Settings.grouping
                    onCheckedChanged: {
                        Settings.grouping = checked
                        if (checked){
                            App.setGrouping("full")
                        }else{
                            App.setGrouping()
                        }
                    }
                }
            } // End of grouping container*/
        }
    }
}