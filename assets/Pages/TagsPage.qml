import bb.cascades 1.2
import bb.system 1.2

import "../Common"
import "../"
import "../Components"

NaviPage {
    id: root
    
    onPopTransitionEnded: {
        {page.destroy(); listView.clearSelection();}
        if (App.loading) App.loading = false
        page.active = false
    }
    
    onCreationCompleted: {
        tabPane.naviPane = naviPane
    }
    
    MyPage{
        id: page
        
        header: TitleHeader {
            titleText: qsTr("All tags") + Retranslate.onLocaleOrLanguageChanged
            counterVisible: (App.tagsSize)
            counterText: App.tagsSize
        }
        loading: App.loading
        loadingText: qsTr("Loading tags...") + Retranslate.onLocaleOrLanguageChanged
        noContentVisible: !App.tagsSize && !App.loading && !App.syncing
        noContentText: qsTr("No tags") + Retranslate.onLocaleOrLanguageChanged
        noContentImageSource: App.whiteTheme ? "asset:///Images/tag_black.png" : "asset:///Images/tag_white.png"
        actionBarAutoHideBehavior: !SizeHelper.nType ? ActionBarAutoHideBehavior.Default : ActionBarAutoHideBehavior.HideOnScroll
        


        onActiveChanged: {
        	App.showTags()
            console.debug("TAGS SIZE:".concat(App.tagsSize))
        }
        
        actions: [
            ActionItem {
                ActionBar.placement: ActionBarPlacement.OnBar
                title: qsTr("Refresh") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///Images/reload.png"
                onTriggered: {
                    App.getTags()
                }
            }
        ]
        
        
        attachedObjects: [
            ComponentDefinition {
                id: tagsBookmarks
                source: "TagsBookmarks.qml"
            }
        ]
        
        ListView {
            id: listView
            
            property variant selectedItems: []
            
            scrollRole: ScrollRole.Main
            leadingVisual: Container {
                visible: (App.tagsSize)
                horizontalAlignment: HorizontalAlignment.Fill
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    
                    Label {
                        text: qsTr("Grouping tags") + Retranslate.onLocaleOrLanguageChanged
                        verticalAlignment: VerticalAlignment.Center
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                    }
                    
                    ToggleButton {
                        verticalAlignment: VerticalAlignment.Center
                        checked: Settings.tagsGrouping
                        onCheckedChanged: {
                            Settings.tagsGrouping = checked
                            App.setTagsGrouping(Settings.tagsGrouping ? "first" : "none")
                        }
                    }
                }
                DropDown {
                    title: qsTr("Sort") + Retranslate.onLocaleOrLanguageChanged
                    selectedIndex: Settings.tagsSortAscending ? 0 : 1
                    options: [
                        Option {
                            text: qsTr("Ascending") + Retranslate.onLocaleOrLanguageChanged
                            value: true
                        },
                        Option {
                            text: qsTr("Descending") + Retranslate.onLocaleOrLanguageChanged
                            value: false
                        }
                    ]
                    onSelectedIndexChanged: {
                        if (selectedValue != Settings.tagsSortAscending){
                            console.log("SELECTED VALUE: ".concat(selectedValue))
                            Settings.tagsSortAscending = selectedValue
                            App.setTagsSortAscending(Settings.tagsSortAscending)
                        }
                    }
                }
            }
            /*
            multiSelectAction: MultiSelectActionItem {
                onTriggered: {
                    console.debug("onTriggered, multiselect")
                }
            }
            multiSelectHandler{
                status: qsTr("None selected") + Retranslate.onLanguageChanged
                
                
                onActiveChanged: {
                    console.debug("onActiveChanged, multiSelectHandler")
                    if (!active){
                        if (Settings.tagsGrouping){
                            //App.setTagsGrouping("first")
                        }
                        listView.selectedItems = listView.selectionList();
                    }else{
                        if (Settings.tagsGrouping){
                            //App.setTagsGrouping("none")
                        }
                    
                    }
                }
                
                actions: [
                    DeleteActionItem {
                        id: deleteAction
                        title: qsTr("Remove") + Retranslate.onLocaleOrLanguageChanged
                        imageSource: "asset:///Images/remove.png"
                        onTriggered: {
                            dialog.show()
                        }
                        attachedObjects: [
                            SystemDialog {
                                id: dialog
                                
                                title: qsTr("Remove tag%1").arg((listView.selectedItems.length > 1) ? qsTr("s") + Retranslate.onLocaleOrLanguageChanged : "") + Retranslate.onLocaleOrLanguageChanged
                                body: qsTr("Are you sure you want to remove tag%1 ?").arg((listView.selectedItems.length > 1) ? qsTr("s") + Retranslate.onLocaleOrLanguageChanged : "") + Retranslate.onLocaleOrLanguageChanged
                                confirmButton{
                                    label: qsTr("Remove") + Retranslate.onLocaleOrLanguageChanged
                                }
                                onFinished: {
                                    if (value == SystemUiResult.ConfirmButtonSelection){
                                        console.debug("listView.selectionList(), ".concat(listView.selectedItems.length))
                                        for (var item in listView.selectedItems){
                                            var indexPath = listView.selectedItems[item]
                                            if (indexPath.length > 1){
                                                console.debug("removing tags")
                                                App.deleteTag(listView.dataModel.data(indexPath).id)
                                            }
                                        }
                                        for (var item in listView.selectedItems){
                                            var indexPath = listView.selectedItems[item]
                                            App.tagsModel.removeAt(indexPath)
                                        }
                                    }else{
                                        console.log("CANCEL CLICKED")
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
            
            onSelectionChanged: {
                if (indexPath.length > 1){
                    console.log("onSelectionChanged, selected items count: ".concat(selectionList().length))
                    if (selectionList().length > 1) {
                        multiSelectHandler.status = qsTr("%1 tags selected").arg(selectionList().length)  + Retranslate.onLocaleOrLanguageChanged;
                    } else if (selectionList().length == 1) {
                        multiSelectHandler.status = qsTr("1 tag selected")  + Retranslate.onLocaleOrLanguageChanged;
                    } else {
                        multiSelectHandler.status = qsTr("None selected")  + Retranslate.onLocaleOrLanguageChanged;
                    }
                }
            }
            */
            
            
            opacity: App.loading ? 0 : 1
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            layout: GridListLayout {
                headerMode: ListHeaderMode.Sticky
                columnCount: 2
                cellAspectRatio: Settings.tagsGrouping ? 4 : 1
            } 
            dataModel: App.tagsModel
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    ListItemTags {
                    }
                }
            ]
            onTriggered: {
                select(indexPath)
                var data = dataModel.data(indexPath)
                if (data){
                    var page = tagsBookmarks.createObject()
                    page.tagName = data.text
                    App.showTagsBookmark(data.bookmark_ids)
                    naviPane.push(page)
                }
            }
        
        } // End of ListView StackListLayout
    
    } 
}