import bb.cascades 1.2
import bb.system 1.2
import com.devpda.tools 1.2

import "Pages"

TabbedPane {
    id: tabPane
    objectName: "tabPane"
    
    
    
    property NavigationPane naviPane
    
    
    function updateSorting(size){
        App.setSortingKeys(Settings.sortingKeys)
        App.setSortAscending(Settings.sortAscending)
    }
    
    
    function prettyDate(time){
        var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
        diff = (((new Date()).getTime() - date.getTime()) / 1000),
        day_diff = Math.floor(diff / 86400);
        
        if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
            return;
        
        return day_diff == 0 && (
        diff < 60 && qsTr("just now") + Retranslate.onLocaleOrLanguageChanged ||
        diff < 120 && qsTr("1 minute ago") + Retranslate.onLocaleOrLanguageChanged ||
        diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
        diff < 7200 && "1 hour ago" ||
        diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
        day_diff == 1 && "Yesterday" ||
        day_diff < 7 && day_diff + " days ago" ||
        day_diff < 31 && Math.ceil( day_diff / 7 ) + " weeks ago";
    }
    
    
    function signedOut(){
        naviPane.pop()
        naviPane.pop()
        loginSheet.open()
    }
    
    
    function refreshTab(){
        switch (tabPane.activeTab){
            case readingTab:
                App.showReading();
                break;
            case favoritesTab:
                App.showFavorites();
                break;
            case archivesTab:
                App.showArchive();
                break;
            case tagsTab:
                App.showTagsBookmark();
                break;
        }
    }
    
    
    function syncingDone(){
        switch (tabPane.activeTab){
            case readingTab:
                App.showReading();
                break;
            case favoritesTab:
                App.showFavorites();
                break;
            case archivesTab:
                App.showArchive();
                break;
        }
    }
    
    
    
    showTabsOnActionBar: true

    Menu.definition: MenuDefinition{
        settingsAction: SettingsActionItem {
            id: settingsAction
            title: qsTr("Settings") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                naviPane.push(settingsPage.createObject())
            }
        }
        helpAction: HelpActionItem {
            id: aboutAction
            title: qsTr("About") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///Images/about.png"
            onTriggered: {
                naviPane.push(aboutPage.createObject())
            }
        }
        actions: [
            ActionItem {
                title: qsTr("Rate app") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///Images/rate.png"
                onTriggered: {
                    invoker.action = "bb.action.OPEN"
                    invoker.target = "sys.appworld"
                    invoker.uri = "appworld://content/35403897"
                    invoker.invoke()
                }
            },
            ActionItem {
                title: qsTr("Tell a Friend") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///Images/ic_bbm.png"
                onTriggered: {
                    //console.log("Here i am")
                    Invite.sendInvite();
                }
            }
        ]
    }
    
    
    tabs: [
        Tab {
            id: readingTab
            
            title: qsTr("Reading List") + Retranslate.onLocaleOrLanguageChanged
            
            delegate: Delegate {
                id: readingTabDelegate
                source: "Pages/ReadingList.qml"
            }
            delegateActivationPolicy: TabDelegateActivationPolicy.ActivateImmediately
            imageSource: "asset:///Images/readingList.png"
            description: qsTr("Reading list") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                if (readingTabDelegate.object != undefined){
                    tabPane.naviPane = readingTabDelegate.object.naviPane
                }
                App.showReading()
            }
        },
        
        
        Tab {
            id: favoritesTab
            
            title: qsTr("Favorites") + Retranslate.onLocaleOrLanguageChanged
            
            delegate: Delegate {
                id: favoritesTabDelegate
                source: "Pages/Favorites.qml"
            } 
            delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
            imageSource: "asset:///Images/favorite.png"
            description: qsTr("Favorite articles") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                if (favoritesTabDelegate.object != undefined){
                    tabPane.naviPane = favoritesTabDelegate.object.naviPane
                }
                App.showFavorites()
            }
        },
        
        
        Tab {
            id: archivesTab
            
            title: qsTr("Archives") + Retranslate.onLocaleOrLanguageChanged
            
            delegate: Delegate {
                id: archivesTabDelegate
                source: "Pages/Archives.qml"
            } 
            delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
            imageSource: "asset:///Images/archives.png"
            description: qsTr("Archived articles") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                if (archivesTabDelegate.object != undefined){
                    tabPane.naviPane = archivesTabDelegate.object.naviPane
                }
                App.showArchive()
            }
        },
        
        Tab {
            id: tagsTab
            
            title: qsTr("Tags") + Retranslate.onLocaleOrLanguageChanged
            
            delegate: Delegate {
                id: tagsTabDelegate
                source: "Pages/TagsPage.qml"
            } 
            delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
            imageSource: "asset:///Images/tag_white.png"
            description: qsTr("All your tags") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                if (tagsTabDelegate.object != undefined){
                    tabPane.naviPane = tagsTabDelegate.object.naviPane
                }
            }
        }
    ]
    
    
    attachedObjects: [
        Invoker {
            id: invoker
            onInvocationFailed: {
                console.log("Unable to invoke")
            }
        },
        ComponentDefinition {
            id: settingsPage
            source: "Pages/SettingsPage.qml"
        },
        ComponentDefinition {
            id: aboutPage
            source: "AboutPage.qml"
        },
        LoginPage {
            id: loginSheet
        },
        Timer {
            id: timer
            interval: 500
            onTimeout: {
                //App.bookmarks(true);
                stop()
            }
        }
    ]
    
    onCreationCompleted: {
        Qt.App = App
        Qt.Api = Api
        Qt.Database = Database
        Qt.Settings = Settings
        Qt.Helper = Helper
        Qt.SizeHelper = SizeHelper
        Qt.ThemeSettings = ThemeSettings
        if (!BBM.allowed)
            BBM.registerApplication();
        App.syncingDone.connect(syncingDone)
        App.updateBookmarkDone.connect(refreshTab)
        App.bookmarksDone.connect(refreshTab)
        App.modelSizeChanged.connect(updateSorting)
        Database.resetAuthDone.connect(signedOut)
        if (!Database.isAuthorized){
            loginSheet.open()
        }else{
            if (Settings.syncStartup || !App.isOffline){
                App.bookmarks(true);
            }else{
                App.showReading()
            }
        }
    }

}