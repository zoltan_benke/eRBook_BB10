import bb.cascades 1.2
import bb.system 1.2
import com.devpda.tools 1.2

import "Common"
import "Components"
import "Dialogs"

MyPage {
    id: root
    
    function updateArticle(bookmark){
        var msg;
        if (bookmark.favorite != article.favorite && bookmark.favorite){
            msg = qsTr("Article favorited") + Retranslate.onLocaleOrLanguageChanged
        }
        else if (bookmark.favorite != article.favorite && !bookmark.favorite){
            msg = qsTr("Article unfavorited") + Retranslate.onLocaleOrLanguageChanged
        }
        else if (bookmark.archive != article.archive && bookmark.archive){
            msg = qsTr("Article archived") + Retranslate.onLocaleOrLanguageChanged
        }
        else if (bookmark.archive != article.archive && !bookmark.archive){
            msg = qsTr("Article unarchived") + Retranslate.onLocaleOrLanguageChanged
        }
        var copy = article
        copy.favorite = bookmark.favorite
        copy.archive = bookmark.archive
        article = copy
        Helper.showToast(msg)
        console.log("CALLED FROM READPAGE.QML")
    }
    
    
    function setContent(content){
        webView.loadHtml(content)
    }
    
    
    function calculatePercent(){
        var current = Math.floor(scrollView.viewableArea.y)
        var total = Math.floor(webLayout.layoutFrame.height)
        var percent = (current*100)/total
        return Math.floor(percent)
    }
    
    function setStyleSheet(readMode, readFamily){
        var styleSheet;
        var backColor;
        if(readMode === 1){
            backColor = "#262626"
            if (readFamily === 0){
                styleSheet = "css/night.css"
            }else if (readFamily === 1){
                styleSheet = "css/night-tnr.css"
            }else if (readFamily === 2){
                styleSheet = "css/night-csm.css"
            }else if (readFamily === 3){
                styleSheet = "css/night-verdana.css"
            }else if (readFamily === 4){
                styleSheet = "css/night-myriad.css"
            }
        }else if (readMode === 2){
            backColor = "#FBF0D9"
            if (readFamily === 0){
                styleSheet = "css/sepia.css"
            }else if (readFamily === 1){
                styleSheet = "css/sepia-tnr.css"
            }else if (readFamily === 2){
                styleSheet = "css/sepia-csm.css"
            }else if (readFamily === 3){
                styleSheet = "css/sepia-verdana.css"
            }else if (readFamily === 4){
                styleSheet = "css/sepia-myriad.css"
            }
        }else{
            backColor = "#f8f8f8"
            if (readFamily === 0){
                styleSheet = "css/day.css"
            }else if (readFamily === 1){
                styleSheet = "css/day-tnr.css"
            }else if (readFamily === 2){
                styleSheet = "css/day-csm.css"
            }else if (readFamily === 3){
                styleSheet = "css/day-verdana.css"
            }else if (readFamily === 4){
                styleSheet = "css/day-myriad.css"
            }
        }
        Settings.backColor = backColor
        webView.settings.userStyleSheetLocation = styleSheet
    }
    
    
    function sharingUrl(){
        if (Settings.useShortener){
            if (!Database.shortUrlExists(article.article.id)){
                App.getShortUrl(article.article.url)
            }else{
                doShareInvoke(Database.shortUrl(article.article.id))
            }
        }else{
            doShareInvoke(article.article.url)
        }
    }
    
    
    function doShareInvoke(url){
        Helper.showToast(qsTr("Sharing...") + Retranslate.onLocaleOrLanguageChanged)
        App.share(article.article.title.concat("\n").concat(url).concat(qsTr(" shared with #eRBook") + Retranslate.onLocaleOrLanguageChanged))
    }
    
    
    function errorShortUrl(errorCode, errorString){
        doShareInvoke(article.article.url)
    }
    
    
    property variant article
    property variant articleContent
    onCreationCompleted: {
        setStyleSheet(Settings.readMode, Settings.readFamily)
        Settings.styleSheetChanged.connect(setStyleSheet)
        App.updateBookmarkDone.connect(updateArticle)
        App.articleContentDone.connect(setContent)
        App.articleContentDone.connect(tabPane.refreshTab)
        if (Settings.fullScreen){
            titleHeader.visibility = ChromeVisibility.Hidden
            root.actionBarAutoHideBehavior = ActionBarAutoHideBehavior.HideOnScroll
        }else{
            titleHeader.visibility = ChromeVisibility.Default
            root.actionBarAutoHideBehavior = ActionBarAutoHideBehavior.Default
        }
    }
    onArticleChanged: {
        titleHeader.titleText = article.article.title
        titleHeader.subtitleText = article.article.url
    }
    header: TitleHeader {
        id: titleHeader
        counterVisible: false
        counterText: calculatePercent()+"%"
    }
    loading: webView.loading
    loadingText: qsTr("Loading content...") + Retranslate.onLocaleOrLanguageChanged
    onActiveChanged: {
        if (active){
            tabPane.naviPane.peeking = false
            if (!Database.isCached(article.article.id)){
                Helper.showToast(qsTr("Downloading content") + Retranslate.onLocaleOrLanguageChanged)
                App.article(article.article.id)
            }else{
                articleContent = Database.getCache(article.article.id)
                webView.loadHtml(articleContent)
            }
        }else{
            tabPane.naviPane.peeking = true
        }
    }
    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            title: qsTr("Back") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                if (!App.updating || !webView.loading){
                    naviPane.pop()
                }
            }
        }
    
    }
    
    
    attachedObjects: [
        ComponentDefinition {
            id: bookmarkTags
            source: "Pages/TagsBookmarkPage.qml"
        }
    ]
    
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: qsTr("Font") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///Images/fontStyle_white.png"
            onTriggered: {
                if (Settings.isPro){
                    fontStyleDialog.visible = !fontStyleDialog.visible
                }else{
                    Helper.showToast(qsTr("Can not change font in free version") + Retranslate.onLocaleOrLanguageChanged)
                }
            }
        },
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: {
                if (Settings.readMode === 1){
                    return qsTr("Night mode") + Retranslate.onLocaleOrLanguageChanged
                }
                else if (Settings.readMode === 2){
                    return qsTr("Sepia mode") + Retranslate.onLocaleOrLanguageChanged
                }
                else{
                    return qsTr("Day mode") + Retranslate.onLocaleOrLanguageChanged
                }
            }
            imageSource: {
                if (Settings.readMode === 1){
                    return "asset:///Images/nightmode.png"
                }
                else if (Settings.readMode === 2){
                    return "asset:///Images/sepiamode.png"
                }
                else{
                    return "asset:///Images/daymode.png"
                }
            }
            onTriggered: {
                if (Settings.isPro){
                    if (Settings.readMode > 2)
                        Settings.readMode = 0
                    Settings.readMode += 1
                }else{
                    Helper.showToast(qsTr("Can not change style in free version") + Retranslate.onLocaleOrLanguageChanged)
                }
            }
        },
        ActionItem {
            title: qsTr("Tag it") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///Images/tag_white.png"
            onTriggered: {
                if (Qt.Settings.isPro){
                    tagDialog.show()
                }else{
                    Qt.Helper.showToast(qsTr("Can't tag articles in free version") + Retranslate.onLocaleOrLanguageChanged)
                }
            }
            attachedObjects: [
                SystemPrompt {
                    id: tagDialog
                    title: qsTr("Tag bookmark") + Retranslate.onLocaleOrLanguageChanged
                    rememberMeChecked: false
                    includeRememberMe: false
                    inputField.emptyText: qsTr("comma separated tags") + Retranslate.onLocaleOrLanguageChanged
                    onFinished: {
                        if (value == SystemUiResult.ConfirmButtonSelection){
                            console.debug("Input text".concat(inputFieldTextEntry()))
                            App.addTagsToBookmark(article.id, inputFieldTextEntry().toLowerCase().trim());
                        }
                    }
                    confirmButton.label: qsTr("Tag it")
                    returnKeyAction: SystemUiReturnKeyAction.Done
                    body: qsTr("Text will be converted to lowercase automatically") + Retranslate.onLocaleOrLanguageChanged
                }
            ]
        },
        ActionItem {
            title: qsTr("Show tags") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///Images/tag_white.png"
            onTriggered: {
                if (Cacher.getBookmarkTags(article.id).length){
                    var tagsPage = bookmarkTags.createObject()
                    tagsPage.bookmarkId = article.id
                    tabPane.naviPane.push(tagsPage)
                }else{
                    Helper.showToast(qsTr("No tags for this bookmark") + Retranslate.onLocaleOrLanguageChanged)
                }
            }
        },
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: qsTr("Full screen") + Retranslate.onLocaleOrLanguageChanged
            imageSource: Settings.fullScreen ? "asset:///Images/fullScreenOff.png" : "asset:///Images/fullScreenOn.png"
            onTriggered: {
                Settings.fullScreen = !Settings.fullScreen
                if (Settings.fullScreen){
                    titleHeader.visibility = ChromeVisibility.Hidden
                    root.actionBarAutoHideBehavior = ActionBarAutoHideBehavior.HideOnScroll
                }else{
                    titleHeader.visibility = ChromeVisibility.Default
                    root.actionBarAutoHideBehavior = ActionBarAutoHideBehavior.Default
                }
            }
        }/*,
          ActionItem {
          ActionBar.placement: ActionBarPlacement.OnBar
          title: Settings.nightMode ? qsTr("Day mode") + Retranslate.onLocaleOrLanguageChanged : qsTr("Night mode") + Retranslate.onLocaleOrLanguageChanged
          imageSource: Settings.nightMode ? "asset:///Images/daymode.png" : "asset:///Images/nightmode.png"
          onTriggered: {
          if (Settings.isPro){
          Settings.readMode += 1
          if (Settings.readMode > 2)
          Settings.readMode = 0
          }else{
          Helper.showToast(qsTr("Can not switch between modes in free version") + Retranslate.onLocaleOrLanguageChanged)
          }
          }
          }*/,
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: qsTr("Open in browser") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///Images/open_in_browser.png"
            onTriggered: {
                invoker.action = "bb.action.OPEN"
                invoker.target = "sys.browser"
                invoker.uri = article.article.url
                invoker.invoke()
            }
        },
        ActionItem {
            enabled: !App.updating
            title: article.archive ? qsTr("Unarchive article") + Retranslate.onLocaleOrLanguageChanged : qsTr("Archive article") + Retranslate.onLocaleOrLanguageChanged
            imageSource: article.archive ? "asset:///Images/readingList.png" : "asset:///Images/no-archive.png"
            onTriggered: {
                App.updateBookmark(article.id, article.favorite ? 1 : 0, article.archive ? 0 : 1)
            }
        },
        ActionItem {
            enabled: !App.updating
            title: article.favorite ? qsTr("Unfavorite article") + Retranslate.onLocaleOrLanguageChanged : qsTr("Favorite article") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///Images/favorite.png"
            onTriggered: {
                App.updateBookmark(article.id, article.favorite ? 0 : 1, article.archive ? 1 : 0)
            }
        },
        ActionItem {
            id: shareAction
            title: qsTr("Share") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///Images/share.png"
            onTriggered: {
                sharingUrl()
            }
        },
        DeleteActionItem {
            title: qsTr("Remove article") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///Images/remove.png"
            onTriggered: {
                removeDialog.show()
            }
            attachedObjects: [
                SystemDialog {
                    id: removeDialog
                    title: qsTr("Remove article") + Retranslate.onLocaleOrLanguageChanged
                    body: qsTr("Are you sure you want to remove this article and its content?") + Retranslate.onLocaleOrLanguageChanged
                    confirmButton{
                        label: qsTr("Remove") + Retranslate.onLocaleOrLanguageChanged
                    }
                    onFinished: {
                        if (value == SystemUiResult.ConfirmButtonSelection){
                            App.removeBookmark(article.id)
                            naviPane.pop()
                        }else{
                            console.log("CANCEL CLICKED")
                        }
                    }
                }
            ]
        }
    ]
    
    
    
    Container {
        id: rootContainer
        layout: DockLayout {}
        background: Color.create(Settings.backColor)
        ScrollView {
            id: scrollView
            onTouch: {
                fontStyleDialog.visible = false
            }
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            scrollViewProperties.pinchToZoomEnabled: false
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            scrollRole: ScrollRole.Main
            WebView {
                id: webView
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                settings.javaScriptEnabled: true
                settings.cookiesEnabled: false
                settings.binaryFontDownloadingEnabled: true
                onMinContentScaleChanged: {
                    scrollView.scrollViewProperties.minContentScale = minContentScale;
                }
                
                onMaxContentScaleChanged: {
                    scrollView.scrollViewProperties.maxContentScale = maxContentScale;
                }
                settings.textAutosizingEnabled: true
                settings.zoomToFitEnabled: true
                settings.activeTextEnabled: true
                settings.devicePixelRatio: 1.0
                settings.viewport: {
                    "width": "device-width",
                    "initial-scale": 1.0
                }
                settings.defaultTextCodecName: "UTF-8"
                settings.background: Color.Transparent
                settings.defaultFontSize: Settings.fontSize
                
                onNavigationRequested: {
                    //WebNavigationRequestAction.Ignore
                    console.debug ("NavigationRequested: " + request.url + " navigationType=" + request.navigationType)
                    if (request.url.toString().indexOf("local:///") !== -1){
                        request.action = WebNavigationRequestAction.Accept
                    }else{
                        request.action = WebNavigationRequestAction.Ignore
                        dialog.url = request.url
                        dialog.show()
                    }
                }
                
                
                attachedObjects: [
                    SystemDialog {
                        id: dialog
                        
                        property string url
                        
                        title: qsTr("Open link") + Retranslate.onLocaleOrLanguageChanged
                        body: qsTr("Open link in a browser?") + Retranslate.onLocaleOrLanguageChanged
                        confirmButton{
                            label: qsTr("Open") + Retranslate.onLocaleOrLanguageChanged
                        }
                        onFinished: {
                            if (value == SystemUiResult.ConfirmButtonSelection){
                                invoker.action = "bb.action.OPEN"
                                invoker.target = "sys.browser"
                                invoker.uri = url
                                invoker.invoke()
                            }
                        }
                    },
                    LayoutUpdateHandler {
                        id: webLayout
                        onLayoutFrameChanged: {
                            console.log("Total height: ".concat(layoutFrame.height))
                            console.log("CURRENT: ".concat(layoutFrame.y))
                        }
                    }
                ]
            }
        } // End of ScrollView
        
        FontStyleDialog {
            id: fontStyleDialog
            verticalAlignment: VerticalAlignment.Bottom
        }
    }
}
