import bb.cascades 1.2


NavigationPane {
    id: navigationPane
    
    property alias naviPane: navigationPane
    property alias peeking: navigationPane.peekEnabled
    
    onPushTransitionEnded: {
        console.log("PUSH ENDED")
        page.active = true
    }
}