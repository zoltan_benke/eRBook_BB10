import bb.cascades 1.2
import com.devpda.tools 1.2


Container {
    id: rootContainer
    layout: DockLayout {}
    
    property int highlightFrameSize: 8
    property string highlightFrameColor: "#".concat(Qt.Settings.activeColor)
    property alias showDivider: divider.visible
    property bool highlight: true
    property alias backColor: rootContainer.background
    property alias invoker: invokation
    
    background: Qt.App.whiteTheme ? Color.create("#f8f8f8") : Color.create("#262626")
    Divider {
        id: divider
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Bottom
    }
    
    
    attachedObjects: [
        Invoker {
            id: invokation
            onInvocationFailed: {
                console.log("Invokation failed")
            }
        }
    ]

    
    Container {
        layout: DockLayout {}
        visible: highlight && rootContainer.ListItem.active || highlight && rootContainer.ListItem.selected
        background: Color.create(highlightFrameColor)
        maxWidth: highlightFrameSize
        minWidth: highlightFrameSize
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Left
    }
    
    Container {
        layout: DockLayout {}
        visible: highlight && rootContainer.ListItem.active || highlight && rootContainer.ListItem.selected
        background: Color.create(highlightFrameColor)
        maxWidth: highlightFrameSize
        minWidth: highlightFrameSize
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Right
    }
    
    
    Container {
        layout: DockLayout {}
        visible: highlight && rootContainer.ListItem.active || highlight && rootContainer.ListItem.selected
        background: Color.create(highlightFrameColor)
        maxHeight: highlightFrameSize
        minHeight: highlightFrameSize
        verticalAlignment: VerticalAlignment.Top
        horizontalAlignment: HorizontalAlignment.Fill
    }
    
    Container {
        layout: DockLayout {}
        visible: highlight && rootContainer.ListItem.active || highlight && rootContainer.ListItem.selected
        background: Color.create(highlightFrameColor)
        maxHeight: highlightFrameSize
        minHeight: highlightFrameSize
        verticalAlignment: VerticalAlignment.Bottom
        horizontalAlignment: HorizontalAlignment.Fill
    }
    
    
    Container {
        id: highlightContainer
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        opacity: 0.0
        background: Color.create("#".concat(Qt.Settings.activeColor))
        
        animations: [
            FadeTransition {
                id: setSelected
                fromOpacity: 0.0
                toOpacity: 1.0
                duration: 200
                target: highlightContainer
            },
            FadeTransition {
                id: setUnselected
                fromOpacity: 1.0
                toOpacity: 0.0
                duration: 200
                target: highlightContainer
            }
        ]
    }
}
