import bb.cascades 1.2



Container {
    
    function open(){
        openAnim.play()
    }
    
    function close(){
        closeAnim.play()
    }
    
    property bool opened: false
    onOpenedChanged: {
        if (!opened){
            console.log("OPENED")
            open()
        }else{
            console.log("CLOSED")
            close()
        }
    }
    layout: DockLayout {}
    horizontalAlignment: HorizontalAlignment.Fill
    preferredHeight: 300
    background: back.imagePaint
    attachedObjects: [
        ImagePaintDefinition {
            id: back
            repeatPattern: RepeatPattern.X
            imageSource: "asset:///Images/patterns/262626.png"
        },
        SequentialAnimation {
            id: openAnim  
            TranslateTransition {
                fromY: 0
                toY: 450
            } 
        },
        SequentialAnimation {
            id: closeAnim
            TranslateTransition {
                fromY: 450
                toY: 0
            }
        }
    ]

    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            preferredHeight: 10
            background: Color.create("#".concat(Settings.activeColor))
        }
        ScrollView {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Fill
            leftPadding: 10
            rightPadding: 10
            topPadding: 5
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            scrollViewProperties.pinchToZoomEnabled: false
            Container {
                
                Label {
                    horizontalAlignment: HorizontalAlignment.Fill
                    text: qsTr("Reading mode:") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        color: Color.White
                        base: SystemDefaults.TextStyles.SubtitleText
                    }
                    bottomMargin: 0
                }
                
                Container {
                    id: modeContainer
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    Container {
                        rightMargin: 25
                        opacity: (Settings.readMode === 0) ? 0.5 : 1
                        ImageButton {
                            horizontalAlignment: HorizontalAlignment.Center
                            bottomMargin: 0
                            defaultImageSource: "asset:///Images/daymode.png"
                            onClicked: {
                                Settings.readMode = 0
                            }
                        }
                        
                        Label {
                            topMargin: 0
                            horizontalAlignment: HorizontalAlignment.Center
                            text: qsTr("Day") + Retranslate.onLocaleOrLanguageChanged
                            textStyle{
                                color: Color.White
                                base: SystemDefaults.TextStyles.SubtitleText
                            }
                        }
                    }
                    
                    
                    Container {
                        rightMargin: 25
                        opacity: (Settings.readMode === 1) ? 0.5 : 1
                        ImageButton {
                            horizontalAlignment: HorizontalAlignment.Center
                            bottomMargin: 0
                            defaultImageSource: "asset:///Images/nightmode.png"
                            onClicked: {
                                Settings.readMode = 1
                            }
                        }
                        
                        Label {
                            topMargin: 0
                            horizontalAlignment: HorizontalAlignment.Center
                            text: qsTr("Night") + Retranslate.onLocaleOrLanguageChanged
                            textStyle{
                                color: Color.White
                                base: SystemDefaults.TextStyles.SubtitleText
                            }
                        }
                    }
                    
                    
                    Container {
                        opacity: (Settings.readMode === 2) ? 0.5 : 1
                        ImageButton {
                            horizontalAlignment: HorizontalAlignment.Center
                            bottomMargin: 0
                            defaultImageSource: "asset:///Images/sepiamode.png"
                            onClicked: {
                                Settings.readMode = 2
                            }
                        }
                        
                        Label {
                            topMargin: 0
                            horizontalAlignment: HorizontalAlignment.Center
                            text: qsTr("Sepia") + Retranslate.onLocaleOrLanguageChanged
                            textStyle{
                                color: Color.White
                                base: SystemDefaults.TextStyles.SubtitleText
                            }
                        }
                    }
                } // End of Mode container
                
                Container {
                    topMargin: 5
                    preferredHeight: 1
                    horizontalAlignment: HorizontalAlignment.Fill
                    background: Color.create("#f8f8f8")
                }
                
                Label {
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Fill
                    text: qsTr("Font family:") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        color: Color.White
                        base: SystemDefaults.TextStyles.SubtitleText
                    }
                    bottomMargin: 0
                }
                
                Container {
                    id: fontFamilyContainer
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    Container {
                        rightMargin: 25
                        opacity: (Settings.readFamily === 0) ? 0.5 : 1
                        ImageButton {
                            horizontalAlignment: HorizontalAlignment.Center
                            bottomMargin: 0
                            defaultImageSource: "asset:///Images/largeFont.png"
                            onClicked: {
                                Settings.readFamily = 0
                            }
                        }
                        
                        Label {
                            topMargin: 0
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            text: "Arial"
                            textStyle{
                                color: Color.White
                                fontFamily: "Arial"
                                base: SystemDefaults.TextStyles.SubtitleText
                            }
                        }
                    }
                    
                    Container {
                        rightMargin: 25
                        opacity: (Settings.readFamily === 1) ? 0.5 : 1
                        ImageButton {
                            horizontalAlignment: HorizontalAlignment.Center
                            bottomMargin: 0
                            defaultImageSource: "asset:///Images/largeFont.png"
                            onClicked: {
                                Settings.readFamily = 1
                            }
                        }
                        
                        Label {
                            topMargin: 0
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "Times New Roman"
                            textStyle{
                                color: Color.White
                                fontFamily: "Times New Roman"
                                base: SystemDefaults.TextStyles.SubtitleText
                            }
                        }
                    }
                    
                    
                    Container {
                        rightMargin: 25
                        opacity: (Settings.readFamily === 2) ? 0.5 : 1
                        ImageButton {
                            horizontalAlignment: HorizontalAlignment.Center
                            bottomMargin: 0
                            defaultImageSource: "asset:///Images/largeFont.png"
                            onClicked: {
                                Settings.readFamily = 2
                            }
                        }
                        
                        Label {
                            topMargin: 0
                            verticalAlignment: VerticalAlignment.Center
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "Comic Sans MS"
                            textStyle{
                                color: Color.White
                                fontFamily: "Comic Sans MS"
                                base: SystemDefaults.TextStyles.SubtitleText
                            }
                        }
                    }
                
                } // End of fontFamilyContainer
                
                Container {
                    topMargin: 5
                    preferredHeight: 1
                    horizontalAlignment: HorizontalAlignment.Fill
                    background: Color.create("#f8f8f8")
                }
                
                Label {
                    horizontalAlignment: HorizontalAlignment.Fill
                    text: qsTr("Font size:") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        color: Color.White
                        base: SystemDefaults.TextStyles.SubtitleText
                    }
                    bottomMargin: 0
                }
                
                Slider {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    fromValue: 32
                    toValue: 60
                    value: Settings.fontSize
                    onImmediateValueChanged: {
                        Settings.fontSize = (immediateValue)
                        console.log("FONT SIZE: ".concat(immediateValue))
                    }
                }
            }
        } // End of scroll view
    } // end of TopBottom container
}